// modifies the votecount value
function mod_votecount(elem, k) {
    count = parseInt(elem.siblings('.count').text()) || 0
    count += k
    elem.siblings('.count').text(count)
}

function toggle_button(elem, vote_type) {
    // Toggles the state of the buttons and updates the label messages
    if (elem.hasClass('off')) {
        elem.removeClass('off');
        change = elem.hasClass('vote-up') ? 1 : -1
    } else {
        elem.addClass('off');
        change = elem.hasClass('vote-up') ? -1 : 1
    }
    if(vote_type == 'upvote' || vote_type == 'downvote') {
        mod_votecount(elem, change)
    }
}

function initialize_rating_bar(rating_bar) {
    let average = parseFloat(rating_bar.attr('data-average')) || 0.0
    // rating_bar.find('.average').css('width', (98 * average / 4) + '%')
    margin_left_bullet_size = 2
    bar_size = 100
    cell_size = bar_size / 5
    rating_bar.find('.average').removeClass('hidden').css('left', bar_size / 2 - margin_left_bullet_size + (cell_size * average) + 'px')
}

function toggle_rate_button(rating_bar, button, vote_type, rating) {

    let count = parseInt(rating_bar.attr('data-count')) || 0
    let average = parseFloat(rating_bar.attr('data-average')) || 0

    if (button.hasClass('on')) {
        // If user already gave this rating, cancel vote
        button.removeClass('on')
        count -= 1
        average = count > 0 ? ( average - rating / (count + 1) ) * ((count + 1) / count) : 0
    } else if(button.siblings('button.on').length > 0) {
        // If user already voted, but gave another rating, cancel previous rating, and consider new one instead
        previousRating = parseInt(button.siblings('button.on').attr('data-rating')) || 0
        button.siblings('button.on').removeClass('on')
        button.addClass('on')
        average = count > 1 ? ( average - previousRating / count ) * (count / (count - 1) ) : 0
        average = count > 0 ? average * (count - 1) / count + (rating / count) : 0
    } else {
        // If user did not vote yet: compute new average
        button.addClass('on')
        count += 1
        average = average * (count - 1) / count + rating / count
    }

    rating_bar.attr('data-count', count)
    rating_bar.find('.count').text(count)
    rating_bar.attr('data-average', average)
    rating_bar.find('.figures .average-rating').text(average.toFixed(1))
    // initialize_rating_bar(rating_bar)
}

function pop_over(elem, msg, cls) {
    var text = '<div></div>'
    var tag = $(text)
    $('#message-box').append(tag)
    tag.addClass('alert alert-' + (cls == 'error' ? 'danger' : cls))
    tag.text(msg)
    tag.delay(3000).fadeOut(1000, function () {
        $(this).remove()
    });
}

function ajax_vote(elem, post_id, vote_type) {
    // Pre-emptitively toggle the button to provide feedback

    // if vote for opposite: set elem to opposite (it will be toggled)
    
    let vote_down_button = elem.siblings('.vote-down')
    let has_down_vote = vote_down_button.length > 0
    let vote_down_button_is_active = !vote_down_button.hasClass('off')

    if(elem.hasClass('vote-up') && has_down_vote && vote_down_button_is_active) {
        elem = elem.siblings('.vote-down')
        vote_type = 'downvote'
    } else if(elem.hasClass('vote-down') && !elem.siblings('.vote-up').hasClass('off')) {
        elem = elem.siblings('.vote-up')
        vote_type = 'upvote'
    }

    toggle_button(elem, vote_type)

    $.ajax('/x/vote/', {
        type: 'POST',
        dataType: 'json',
        data: {post_id: post_id, vote_type: vote_type},
        success: function (data) {
            if (data.status == 'error') { // Soft failure, like not logged in
                pop_over(elem, data.msg, data.status) // Display popover only if there was an error
                toggle_button(elem, vote_type) // Untoggle the button if there was an error
            } else if(data.status == 'warning') {
                pop_over(elem, data.msg, 'error')
            }

        },
        error: function () { // Hard failure, like network error
            pop_over(elem, 'Unable to submit vote!', 'danger');
            toggle_button(elem, vote_type);
        }
    });
}


function ajax_rate(rating_bar, button, post_id, vote_type, rating) {

    if(rating_bar != null && button != null) {
        toggle_rate_button(rating_bar, button, vote_type, rating)
    }

    $.ajax('/x/rate/', {
        type: 'POST',
        dataType: 'json',
        data: {post_id: post_id, vote_type: vote_type, rating: rating},
        success: function (data) {
            if (data.status == 'error') { // Soft failure, like not logged in
                pop_over(rating_bar, data.msg, data.status) // Display popover only if there was an error
                if(rating_bar != null && button != null) {
                    toggle_rate_button(rating_bar, button, vote_type, rating) // Untoggle the button if there was an error
                }
            } else if(data.status == 'warning') {
                pop_over(rating_bar, data.msg, 'error')
            }

        },
        error: function () { // Hard failure, like network error
            pop_over(rating_bar, 'Unable to submit vote!', 'error');
            if(rating_bar != null && button != null) {
                toggle_rate_button(rating_bar, button, vote_type, rating)
            }
        }
    });
}


function deactivate_ratings(event) {
	let post_body = $("#post-details .root .post-body")
	let data_vote_types = post_body.attr('data-vote-types')
	if(data_vote_types != null) {

	    let vote_types = data_vote_types.split(',').filter( (e)=> e.length > 0 )
	    let quality_only = vote_types.length == 1 && vote_types[0] == 'quality'
	    if(quality_only) {
	        return
	    }

	    let active_ratings = $('.ratings.active')
	    if(active_ratings.length > 0 && !active_ratings.has(event.target).length > 0 && !active_ratings.is(event.target)) {
	        active_ratings.removeClass('active')
	    }	
	}
}

function initialize_ratings() {

    // Vote submission.
    $('.vote').each(function () {

        $($(this)).click(function () {
            var elem = $(this);
            var post_id = elem.parent().attr('data-post_id');
            var vote_type = elem.attr('data-type')
            ajax_vote(elem, post_id, vote_type);
        });
    });


    $('.ratings').click(function(event) {
        deactivate_ratings(event)
        let ratings = $(this)
        if(!ratings.hasClass('active')) {
            ratings.addClass('active')
        }

    })

    $('.rating').mouseenter(function(event) {

        let target = $(event.target)
        let rating_bar = target.parents('.rating-bar')
        rating_bar.find('.button_value').text(target.attr('data-rating')).addClass('show')

    }).mouseleave(function(event) {

        let rating_bar = $(event.target).parents('.rating-bar')
        rating_bar.find('.button_value').removeClass('show')
    })

    $(window).click(function(event) {
        deactivate_ratings(event)
    })

    $('.rating-bar button.rating').click(function () {
        var button = $(this);
        var rating_bar = button.parents('.rating-bar');
        var post_id = rating_bar.attr('data-post-id');
        var vote_type = rating_bar.attr('data-type');
        var rating = parseInt(button.attr('data-rating')) || 0;
        ajax_rate(rating_bar, button, post_id, vote_type, rating);
        button.blur();
    });

    $('.rating-bar').each(function (elem) {
        rating_bar = $(this)
        user_rating = parseInt(rating_bar.attr('data-user-rating'))
        if(user_rating != null && Number.isFinite(user_rating)) {
            rating_bar.find('button[data-rating="' + user_rating + '"]').addClass('on')
        }
        // initialize_rating_bar(rating_bar);
    });

}
