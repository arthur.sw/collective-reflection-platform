// using jQuery
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

csrftoken = getCookie('csrftoken');

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

$.ajaxSetup({
    crossDomain: false, // obviates need for sameOrigin test
    beforeSend: function (xhr, settings) {
        if (!csrfSafeMethod(settings.type)) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});

// Triggered on post moderation.
function moderate_post(elem) {
    var post_id = elem.attr('data-value')
    var modpanel = $('#modpanel')
    if (modpanel.length > 0) {
        $('#modpanel').remove()
    } else {
        var page = $('<div id="modpanel"></div>').load("/local/moderate/post/" + post_id + "/")
        elem.parent().parent().after(page)
    }
}

// Triggered on user moderation.
function moderate_user(elem) {
    var user_id = elem.attr('data-value')
    var modpanel = $('#modpanel')
    if (modpanel.length > 0) {
        $('#modpanel').remove()
    } else {
        // Passing a data forces a POST request.
        var page = $('<div id="modpanel"></div>').load("/local/moderate/user/" + user_id + "/")
        // Insert the result
        elem.parent().parent().after(page)
    }
}

// Comments by authenticated users.
function add_comment(elem) {

    // remove comment body if exists.
    $("#comment-row").remove();

    var post_id = elem.attr('data-value')
    var container = elem.parent().parent()

    var csrf_html = $('#csrf_token').find("input[name='csrfmiddlewaretoken']").parent().html()

    container.after('<div id="comment-row">\
    <form id="comment-form" role="form" action="/p/new/comment/' + post_id + '/" method="post">' + csrf_html + '\
        <div class="form-group">\
        <div class="wmd-panel">\
            <div id="wmd-button-bar-2"></div>\
            <textarea class="wmd-input-2" id="wmd-input-2"  name="content" rows="3"></textarea></div> \
        <div class="comment-submit flex-row flex-end"> \
            <a class="btn btn-warning pull-right" onclick="javascript:obj=$(\'#comment-row\').remove();"><i class="icon-remove"></i> Cancel</a>    \
            <a class="btn btn-success" href=\'javascript:document.forms["comment-form"].submit()\'><i class="icon-comment"></i> Add Comment</a>          \
        </div>       \
    </form>            \
    </div>'
    )

    var converter = new Markdown.Converter();
    var editor = new Markdown.Editor(converter, '-2');
    editor.run();

}

function add_comment_anon(elem) {
    container = elem.closest("div")
    elem.css("background-color", "red");
    $("#comment-box").remove();
    container.append('' +
        '<div id="comment-box" class="alert alert-warning">Please <a class="alert-link" href="/accounts/login/">log in</a> to comment</div>'
     )
}

function title_format(row) {
    link = '<a href="' + row.url + '"/>' + row.text + '</a><div class="in">' + row.context + ' by <i>' + row.author + '</i></div>';
    return link
}

function initialize_augmented_links() {

    // Convert all links to posts to augmented links 
    // = Add score / average rating and number of votes of the target post,
    //   create a popover card with more info: vote info, title and author card
    $('a[data-post-id]').each(function() {
        let elem = $(this)

        var jqxhr = $.ajax( {
            url: '/ajax/get_augmented_link/',
            data: {
                'link_content': elem.text(),
                'post_id': elem.attr('data-post-id')
            },
            dataType: 'html'
        } )
        .done(function(jqXHR, textStatus, errorThrown) {
            elem.empty()
            elem.append(jqXHR)
        })
        .fail(function(jqXHR, textStatus, errorThrown) {
            console.log(jqXHR)
            console.log(textStatus)
            console.log(errorThrown)
        })
        .always(function(jqXHR, textStatus, errorThrown) {
        });

    })
}

function initialize_links_to_posts() {
    $('.link-to-post').click((event)=> {
        event.preventDefault()
        let post_url = $(event.target).siblings('.post-url')
        let input = post_url.find('input')
        
        post_url.addClass('show')
        
        let url = location.origin + input.val()
        if(!input.val().startsWith(location.origin)) {
            input.val(url)
        }
        
        input.get(0).select()

        /* Copy the text inside the text field */
        document.execCommand("copy");

        post_url.removeClass('show')
        /* Alert the copied text */
        pop_over(event.target, 'Link copied to the clipboard: ' + url, 'success')
    })
}

$(document).ready(function () {

    var tooltip_options = {};


    var wmd = $('#wmd-input')
    if (wmd.length) {
        var converter = new Markdown.Converter();
        var editor = new Markdown.Editor(converter);
        editor.run();
    }


    var searchform = $("#searchform")

    console.log("LANGUAGE_CODE:");
    console.log(LANGUAGE_CODE);
    console.log("REQUEST_SUBDOMAIN:");
    console.log(REQUEST_SUBDOMAIN);

    if (searchform.length > 0) {
        searchform.focus();

        // Add the search functionality
        searchform.select2({
            placeholder: ( LANGUAGE_CODE == 'fr' ? "Rechercher..." : "Search...") ,
            minimumInputLength: 3,
            ajax: {
                url: TITLE_SEARCH_URL,
                dataType: 'json',
                data: function (term, page) {
                    return {
                        q: term, // search term
                        page_limit: 10
                    };
                },
                results: function (data, page) {
                    console.log(data.items)
                    console.log(page)
                    return {results: data.items};
                }
            },

            formatResult: title_format,

            dropdownCssClass: "bigdrop",
            escapeMarkup: function (m) {
                return m;
            }
        })
        searchform.on("change", function (e) {
            window.location = e.val
        })
    }


    // This gets triggered only if tag recommendation
    // becomes necessary.
    var tagval = $("#id_tag_val")

    if (tagval.length > 0) {
        // tagval.removeClass("textinput textInput form-control")
        // tagval.width("100%")

        var tag_list = $.ajax({
            url: "/local/search/tags/",
            dataType: 'json',
            success: function (response) {
                tagval.select2({
                    tags: response
                });
            }
        });
    }

    // Register tooltips.
    $('.tip').tooltip(tooltip_options)

    // Register comment adding.
    if (USER_ID) {

        // Authenticated user actions.
        $('.add-comment').each(function () {
            $(this).click(function (event) {
                event.preventDefault();
                add_comment($(this));
            });
        });

        // Moderator actions.
        $('.mod-post').each(function () {
            $(this).click(function () {
                moderate_post($(this));
            });
        });

        // Moderator actions.
        $('.mod-user').each(function () {
            $(this).click(function () {
                moderate_user($(this));
            });
        });

    } else {

        // Anonymous user actions.
        $('.add-comment').each(function () {
            $(this).click(function (event) {
                event.preventDefault();
                add_comment_anon($(this));
            });
        });
    }


    initialize_ratings()
    initialize_chart()
    initialize_survey()
    initialize_polls()
    initialize_augmented_links()
    initialize_links_to_posts()


        // let chartColors = {
        //     red: 'rgb(255, 99, 132)',
        //     orange: 'rgb(255, 159, 64)',
        //     yellow: 'rgb(255, 205, 86)',
        //     green: 'rgb(75, 192, 192)',
        //     blue: 'rgb(54, 162, 235)',
        //     purple: 'rgb(153, 102, 255)',
        //     grey: 'rgb(201, 203, 207)'
        // };

        // let backgroundColors = [
        //     chartColors.red,
        //     chartColors.orange,
        //     chartColors.yellow,
        //     chartColors.green,
        //     chartColors.blue,
        //     chartColors.purple,
        //     chartColors.grey,
        // ];

        // var ctx = survey_canvas.getContext('2d');

        // var chart = new Chart(ctx, {
        //     // The type of chart we want to create
        //     type: 'bar',

        //     // The data for our dataset
        //     data: {
        //         labels: labels,
        //         datasets: [{
        //             label: type,
        //             backgroundColor: backgroundColors,
        //             data: data,
        //         }]
        //     },

        //     // Configuration options go here
        //     options: {
        //         maintainAspectRatio: false,
        //         legend: {
        //             // display: false
        //             position: 'right'
        //         },
        //         // legendCallback: function(chart) {
        //         //     let container = $('<div>')
        //         //     let i = 0
        //         //     // Return the HTML string here.
        //         //     for(let item of labels) {
        //         //         let div = $('<div>')
        //         //         let color = $('<div>')
        //         //         color.css({ width: 20, height: 20, backgroundColor: backgroundColors[i], display: 'inline-block' })
        //         //         let text = $('<span>')
        //         //         text.text(item)
        //         //         div.append(color)
        //         //         div.append(text)
        //         //         container.append(div)
        //         //         i++
        //         //     }
        //         //     return container.get(0)
        //         // }
        //     }
        // });

    // Checkboxes
});


window.set_debug_ajax = function(debug) {

    var jqxhr = $.ajax( {
        type: 'POST',
        url: '/ajax/debug/',
        data: {
            'debug': debug,
        }
    } )
    .done(function(jqXHR, textStatus, errorThrown) {
        console.log('ajax debug set')
    })
    .fail(function(jqXHR, textStatus, errorThrown) {
        console.log(jqXHR)
        console.log(textStatus)
        console.log(errorThrown)
    });
}

