function initialize_survey() {

    let check_enable_deadline = function() {

        if($('#id_enable_deadline').prop("checked")) {
            $('#div_id_close_date').show()
            $('#div_id_close_time').show()
        } else {
            $('#div_id_close_date').hide()
            $('#div_id_close_time').hide()
        }
    }

    let check_type_is_survey = function() {
        let type_select = $('#div_id_post_type select')
        let value = type_select.val()
        if( type_select.find('option[value=' + value + ']').html() == 'Survey') {
            $('#div_id_enable_deadline').show()
            check_enable_deadline()
            
        } else {
            $('#div_id_enable_deadline').hide()
            $('#div_id_close_date').hide()
            $('#div_id_close_time').hide()
        }
    }
    
    check_type_is_survey()

    $('#div_id_post_type select').change(function(event) {
        check_type_is_survey()
    })

    $('#id_enable_deadline').change(function() {
        check_enable_deadline()
    });

    let today = new Date()

    let post_close_date_input = $('#div_id_close_date .dateinput')
    let post_close_time_input = $('#div_id_close_time .timeinput')
    
    $('#id_time_zone').val(Intl.DateTimeFormat().resolvedOptions().timeZone)

    if(post_close_date_input.length > 0 && post_close_time_input.length > 0) {

        let post_close_date_value = post_close_date_input.val()
        
        if(post_close_date_value == '') {
            post_close_date_value = new Date()
            post_close_date_value.setDate(today.getDate() + 1)
        } else {
            post_close_date_value = new Date(post_close_date_value)
        }

        let post_close_time_value = post_close_time_input.val()
        let post_close_time_date = new Date()

        if(post_close_time_value != '') {
            let [hours, minutes, seconds] = post_close_time_input.val().split(':')
            post_close_time_date.setUTCHours(hours, minutes, seconds)
        }

        post_close_date_input.datepicker({
            // 'format': 'm/d/yyyy',
            autoHide: true,
            date: post_close_date_value,
            startDate: today
        }).datepicker('setDate', post_close_date_value);

        post_close_time_input.timepicker({
           timeFormat: 'g:i A'
        }).timepicker('setTime', post_close_time_date);

    }

    // TODO: fix timezone by storing timezone by user
    // Convert UTC time to local time
    $('.convert-time').each(function() {
        let text = $(this).text()

        let m;
        const regex = /\d+:\d+ UTC/gm;
        while ((m = regex.exec(text)) !== null) {
            // This is necessary to avoid infinite loops with zero-width matches
            if (m.index === regex.lastIndex) {
                regex.lastIndex++;
            }
            
            // The result can be accessed through the `m`-variable.
            m.forEach((match, groupIndex) => {
                console.log(match);
                let [hours, minutes] = match.replace(' UTC', '').split(':')
                let date = new Date()
                date.setUTCHours(hours, minutes)
                time_string = date.toLocaleTimeString('en-US')
                text = text.replace(match, time_string + ' ' + Intl.DateTimeFormat().resolvedOptions().timeZone)
            });
        }

        $(this).text(text)
    })
}
