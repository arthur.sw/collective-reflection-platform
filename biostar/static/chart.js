
function truncate( string, n, useWordBoundary ){
    if (string.length <= n) { return string; }
    var subString = string.substr(0, n-1);
    return (useWordBoundary 
       ? subString.substr(0, subString.lastIndexOf(' ')) 
       : subString) + "...";
};

function initialize_chart_from_type(type) {

    let survey_canvas = document.getElementById('survey-chart-' + type)

    if(survey_canvas != null) {

        let labels = []
        let data = []

        $('.answer').each(function(index, element) {
            let post_main = $(element).find('.main')
            let content_text = post_main.find('.content-container .content span[itemprop="text"]')
            let title = content_text.find('h1, h2, h3, h4, h5, h6').first().text()
            
            if(title.length == 0) {
                title = truncate(content_text.children().first().text(), 50, true)
            }

            labels.push(title)
            
            if(type == 'votes') {
                let vote = post_main.find('.count[itemprop="voteCount"]')
                data.push( Math.max(0, parseInt( vote.text() )) )
            } else {
                let rating = post_main.find('.rating-bar.left.' + type.replace(/-/g, '_'))
                data.push( parseFloat( rating.attr('data-average') ) )
            }
            
        })

        if(data.length == 0) {
            return
        }

        let chartColors = {
            red: 'rgb(255, 99, 132)',
            orange: 'rgb(255, 159, 64)',
            yellow: 'rgb(255, 205, 86)',
            green: 'rgb(75, 192, 192)',
            blue: 'rgb(54, 162, 235)',
            purple: 'rgb(153, 102, 255)',
            grey: 'rgb(201, 203, 207)'
        };

        let backgroundColors = [
            chartColors.red,
            chartColors.orange,
            chartColors.yellow,
            chartColors.green,
            chartColors.blue,
            chartColors.purple,
            chartColors.grey,
        ];

        var ctx = survey_canvas.getContext('2d');

        var chart = new Chart(ctx, {
            // The type of chart we want to create
            type: 'doughnut',

            // The data for our dataset
            data: {
                labels: labels,
                datasets: [{
                    label: type,
                    backgroundColor: backgroundColors,
                    data: data,
                }]
            },

            // Configuration options go here
            options: {
                maintainAspectRatio: false,
                legend: {
                    // display: false
                    position: 'right'
                },
                // legendCallback: function(chart) {
                //     let container = $('<div>')
                //     let i = 0
                //     // Return the HTML string here.
                //     for(let item of labels) {
                //         let div = $('<div>')
                //         let color = $('<div>')
                //         color.css({ width: 20, height: 20, backgroundColor: backgroundColors[i], display: 'inline-block' })
                //         let text = $('<span>')
                //         text.text(item)
                //         div.append(color)
                //         div.append(text)
                //         container.append(div)
                //         i++
                //     }
                //     return container.get(0)
                // }
            }
        });

        // let legend = chart.generateLegend()
        // $(survey_canvas).parent().after(legend)
    }

}

function initialize_chart() {

    if($('.post-body.root-post').hasClass('up-down-votes')) {
        initialize_chart_from_type('votes')
    } else {
        initialize_chart_from_type('level-of-agreement')
        initialize_chart_from_type('relevance')
        initialize_chart_from_type('quality')
    }

    let chart_tab = $('#chart-tab')
    if(chart_tab.length > 0) {
        let active_tab = chart_tab.attr('data-active')
        if(active_tab) {
            $('#' + active_tab.replace(/_/g, '-') + '-tab').tab('show')
        }
    }

}