from __future__ import absolute_import
from django.conf import settings

from .celery import app

import logging

from celery.utils.log import get_task_logger

logger = get_task_logger(__name__)

@app.task
# Close surveys
def close_survey(post, user):
    from biostar.apps.posts.models import Post
    from biostar.apps.users.models import User
    from biostar.apps.util import html

    print('close_survey')

    logger.info("close survey %s" % (post.id))
    
    robot = User.objects.get(name='bot')

    post.update(status=Post.CLOSED)
    content = html.render(name="messages/close_survey.html", post=post)
    comment = Post(content=content, type=Post.COMMENT, parent=post, author=user)
    comment.save()
