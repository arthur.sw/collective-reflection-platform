"""
Constants that may be used in multiple packages
"""
try:
    from collections import OrderedDict
except ImportError, exc:
    # Python 2.6.
    from ordereddict import OrderedDict


from django.utils.translation import ugettext_lazy

from django.utils.timezone import utc
from datetime import datetime

# Message type selector.
LOCAL_MESSAGE, EMAIL_MESSAGE, NO_MESSAGES, DEFAULT_MESSAGES, ALL_MESSAGES = range(5)

MESSAGING_MAP = OrderedDict([
    (DEFAULT_MESSAGES, ugettext_lazy("default"),),
    (LOCAL_MESSAGE, ugettext_lazy("local messages"),),
    (EMAIL_MESSAGE, ugettext_lazy("email"),),
    (ALL_MESSAGES, ugettext_lazy("email for every new thread (mailing list mode)"),),
])

MESSAGING_TYPE_CHOICES = MESSAGING_MAP.items()

# Connects a user sort dropdown word to a data model field.
USER_SORT_MAP = OrderedDict([
    (ugettext_lazy("recent visit"), "-profile__last_login"),
    (ugettext_lazy("reputation"), "-score"),
    (ugettext_lazy("date joined"), "profile__date_joined"),
    #("number of posts", "-score"),
    (ugettext_lazy("activity level"), "-activity"),
])

# These are the fields rendered in the user sort order drop down.
USER_SORT_FIELDS = USER_SORT_MAP.keys()
USER_SORT_DEFAULT = USER_SORT_FIELDS[0]

USER_SORT_INVALID_MSG = ugettext_lazy("Invalid sort parameter received")

# Connects a post sort dropdown word to a data model field.
POST_SORT_MAP = OrderedDict([
    (ugettext_lazy("update"), "-lastedit_date"),
    (ugettext_lazy("views"), "-view_count"),
    (ugettext_lazy("followers"), "-subs_count"),
    (ugettext_lazy("answers"), "-reply_count"),
    (ugettext_lazy("bookmarks"), "-book_count"),
    (ugettext_lazy("votes"), "-vote_count"),
    (ugettext_lazy("rank"), "-rank"),
    (ugettext_lazy("creation"), "-creation_date"),
])

DEBATE_SORT_MAP = OrderedDict([
    ("types", ugettext_lazy("types")),
    ("quality", ugettext_lazy("quality")),
    ("relevance", ugettext_lazy("relevance")),
    ("level_of_agreement", ugettext_lazy("level of agreement")),
    ("votes", ugettext_lazy("votes")),
    ("creation_dates", ugettext_lazy("creation dates"))
])

# These are the fields rendered in the post sort order drop down.
POST_SORT_FIELDS = POST_SORT_MAP.keys()
POST_SORT_DEFAULT = POST_SORT_FIELDS[0]

POST_SORT_INVALID_MSG = ugettext_lazy("Invalid sort parameter received")

# Connects a word to a number of days
POST_LIMIT_MAP = OrderedDict([
    (ugettext_lazy("all time"), 0),
    (ugettext_lazy("today"), 1),
    (ugettext_lazy("this week"), 7),
    (ugettext_lazy("this month"), 30),
    (ugettext_lazy("this year"), 365),

])

# These are the fields rendered in the time limit drop down.
POST_LIMIT_FIELDS = POST_LIMIT_MAP.keys()
POST_LIMIT_DEFAULT = POST_LIMIT_FIELDS[0]

POST_LIMIT_INVALID_MSG = ugettext_lazy("Invalid limit parameter received")


def now():
    return datetime.utcnow().replace(tzinfo=utc)


