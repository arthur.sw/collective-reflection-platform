__author__ = 'ialbert'
import json, traceback, logging
from braces.views import JSONResponseMixin
from biostar.apps.posts.models import Post, Vote
from biostar.apps.users.models import User
from django.views.generic import View
from django.shortcuts import render_to_response, render
from django.http import HttpResponse, HttpResponseRedirect, HttpResponsePermanentRedirect, Http404
from functools import partial
from django.db import transaction
from django.db.models import Q, F
from django.conf import settings

from django.utils.translation import ugettext as _
from django.utils.translation import ungettext
from django.utils.translation import ugettext_lazy

import functools


debug_ajax = False

if settings.DEBUG:
    import pdb

    def ajax_debug(func):
        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            global debug_ajax
            if debug_ajax:
                print('debug: ' + func.__name__ + ', press s to jump into function, n to step one line, c to continue.')
                pdb.Pdb(skip=['django.*', 'gevent.*']).set_trace()
            return func(*args, **kwargs)
        return wrapper
else:
    def ajax_debug(func):
        return func

def set_debug_ajax(request):
    "Set debug ajax"
    if settings.DEBUG:
        global debug_ajax
        debug_ajax = request.POST.get('debug', False) == 'true'
    return ajax_success("")

def json_response(adict, **kwd):
    """Returns a http response in JSON format from a dictionary"""
    return HttpResponse(json.dumps(adict), **kwd)


logger = logging.getLogger(__name__)


def ajax_msg(msg, status, **kwargs):
    payload = dict(status=status, msg=msg)
    payload.update(kwargs)
    return json_response(payload)


ajax_success = partial(ajax_msg, status='success')
ajax_error = partial(ajax_msg, status='error')
ajax_warning = partial(ajax_msg, status='warning')


class ajax_error_wrapper(object):
    """
    Used as decorator to trap/display  errors in the ajax calls
    """

    def __init__(self, f):
        self.f = f

    def __call__(self, request):
        try:
            if request.method != 'POST':
                return ajax_error(_('POST method must be used.'))

            if not request.user.is_authenticated():
                return ajax_error(_('You must be logged in to perform this action'))

            value = self.f(request)
            return value
        except Exception, exc:
            traceback.print_exc()
            return ajax_error(_('Error: %s') % exc)


POST_TYPE_MAP = dict(vote=Vote.UP, upvote=Vote.UP, downvote=Vote.DOWN, level_of_agreement=Vote.LEVEL_OF_AGREEMENT, quality=Vote.QUALITY, bookmark=Vote.BOOKMARK, accept=Vote.ACCEPT, relevance=Vote.RELEVANCE)

@transaction.atomic
def perform_vote(post, user, vote_type):

    warning = False

    # Only maintain one vote for each user/post pair.
    vote_types = [Vote.UP, Vote.DOWN] if vote_type == Vote.UP or vote_type == Vote.DOWN else [vote_type]
    votes = Vote.objects.filter(author=user, post=post, type__in=vote_types)
    if votes:
        vote = votes[0]
        msg = _("%s removed") % vote.get_type_display()
        change = -1 if vote.type != Vote.DOWN else +1
    else:
        change = +1 if vote_type != Vote.DOWN else -1
        vote = Vote.objects.create(author=user, post=post, type=vote_type)
        msg = _("%s added") % vote.get_type_display()
        if vote_type == Vote.DOWN:
            warning = True
            msg = _("You must explain the reasons of your down vote with one or more comments")

    if post.author != user and (vote.type == Vote.UP or vote.type == Vote.DOWN):
        # Update the user reputation only if the author is different.
        User.objects.filter(pk=post.author.id).update(score=F('score') + change)

    # The thread score represents all votes in a thread
    Post.objects.filter(pk=post.root_id).update(thread_score=F('thread_score') + change)

    if vote.type == Vote.BOOKMARK:
        # Apply the vote
        Post.objects.filter(pk=post.id).update(book_count=F('book_count') + change)
        Post.objects.filter(pk=post.id).update(subs_count=F('subs_count') + change)
        Post.objects.filter(pk=post.root_id).update(subs_count=F('subs_count') + change)

    elif vote_type == Vote.ACCEPT:
        if change > 0:
            # There does not seem to be a negation operator for F objects.
            # Post.objects.filter(pk=post.id).update(vote_count=F('vote_count') + change, has_accepted=True)
            Post.objects.filter(pk=post.id).update(has_accepted=True)

            Post.objects.filter(pk=post.root_id).update(has_accepted=True)
        else:
            # Post.objects.filter(pk=post.id).update(vote_count=F('vote_count') + change, has_accepted=False)
            Post.objects.filter(pk=post.id).update(has_accepted=False)

            # Only set root as not accepted if there are no accepted siblings
            if Post.objects.exclude(pk=post.root_id).filter(root_id=post.root_id, has_accepted=True).count() == 0:
                Post.objects.filter(pk=post.root_id).update(has_accepted=False)
    else:
        Post.objects.filter(pk=post.id).update(vote_count=F('vote_count') + change)
        Post.objects.filter(pk=post.id).update(quality=F('quality') + change)

    # Clear old votes.
    if votes:
        votes.delete()

    return (msg, warning)

@transaction.atomic
def perform_rating(post, user, vote_type, value, cancelled_previous_vote=False):

    # Only maintain one vote for each user/post pair.

    votes = Vote.objects.filter(author=user, post=post, type=vote_type)
    
    rating_to_reputation = dict([(0, -1), (1, 0), (2, 0), (3, 1), (4, 2)])

    vote_was_same = False

    warning = False

    if votes:
        vote = votes[0]
        msg = _("%s removed") % vote.get_type_display()
        reputation_offset = -rating_to_reputation[vote.value]
        count_offset = -1
        vote_was_same = vote.value == value
    else:
        vote = Vote.objects.create(author=user, post=post, type=vote_type, value=value)
        msg = _("%s added") % vote.get_type_display()
        reputation_offset = rating_to_reputation[value]
        count_offset = 1
        if vote_type == Vote.QUALITY and value == 0:
            warning = True
            msg = _("You must explain the reasons of your vote with one or more comments")

    if post.author != user and vote.type == Vote.QUALITY:
        # Update the user reputation only if the author is different.
        User.objects.filter(pk=post.author.id).update(score=F('score') + reputation_offset)

    # The thread score represents all votes in a thread
    Post.objects.filter(pk=post.root_id).update(thread_score=F('thread_score') + count_offset)

    post = Post.objects.get(pk=post.id)

    post.vote_count = count_offset

    if vote.type == Vote.RELEVANCE:
        # TODO: use F expressions
        # relevance_count = F('relevance_count')
        # relevance = F('relevance')
        post.relevance_count = post.relevance_count + count_offset
        if count_offset > 0:
            post.relevance = (post.relevance_count-1) * post.relevance / float(post.relevance_count) + value / float(post.relevance_count)
        else:
            post.relevance = ( post.relevance - vote.value / float(post.relevance_count + 1 ) ) * ( post.relevance_count + 1 ) / float(post.relevance_count) if post.relevance_count > 0 else 0
        post.save()
    elif vote.type == Vote.LEVEL_OF_AGREEMENT:
        post.level_of_agreement_count += count_offset
        if count_offset > 0:
            post.level_of_agreement = (post.level_of_agreement_count-1) * post.level_of_agreement / float(post.level_of_agreement_count) + value / float(post.level_of_agreement_count)
        else:
            post.level_of_agreement = ( post.level_of_agreement - vote.value / float(post.level_of_agreement_count + 1 ) ) * ( post.level_of_agreement_count + 1 ) / float(post.level_of_agreement_count) if post.level_of_agreement_count > 0 else 0
        post.save()
    elif vote.type == Vote.QUALITY:
        post.quality_count += count_offset
        if count_offset > 0:
            post.quality = (post.quality_count-1) * post.quality / float(post.quality_count) + value / float(post.quality_count)
        else:
            post.quality = ( post.quality - vote.value / float(post.quality_count + 1 ) ) * ( post.quality_count + 1 ) / float(post.quality_count) if post.quality_count > 0 else 0
        post.save()

    # Clear old votes.
    if votes:
        votes.delete()

        if cancelled_previous_vote:
            raise RuntimeError(_('Cancelled one vote but another vote exists.'))

        # If the previous vote was different: we cancelled the old one, now we must consider the new one
        if not vote_was_same:
            return perform_rating(post, user, vote_type, value, True)

    return (msg, warning)


@transaction.atomic
def perform_survey_vote(post, user, vote_type):

    warning = False

    votes = Vote.objects.filter(author=user, post=post, type__in=vote_types)
    if votes:
        votes.delete()
    else:
        vote = Vote.objects.create(author=user, post=post, type=vote_type)

    return (msg, warning)

@ajax_error_wrapper
@ajax_debug
def vote_handler(request):
    "Handles all voting on posts"


    user = request.user
    vote_type = request.POST['vote_type']
    vote_type = POST_TYPE_MAP[vote_type]
    post_id = request.POST['post_id']

    # Check the post that is voted on.
    post = Post.objects.get(pk=post_id)

    if post.root.is_closed_survey():
        return ajax_error(_("The survey is now closed. It is not possible to vote in this thread anymore."))

    if post.author == user and (vote_type == Vote.UP or vote_type == Vote.DOWN):
        return ajax_error(_("You can't upvote your own post."))

    #if post.author == user and vote_type == Vote.ACCEPT:
    #    return ajax_error("You can't accept your own post.")

    if post.root.author != user and vote_type == Vote.ACCEPT:
        return ajax_error(_("Only the person asking the question may accept this answer."))


    with transaction.atomic():
        msg, warning = perform_vote(post=post, user=user, vote_type=vote_type)

    return ajax_warning(msg) if warning else ajax_success(msg)

@ajax_error_wrapper
@ajax_debug
def rate_handler(request):
    "Handles all rating on posts"

    user = request.user
    vote_type = request.POST.get('vote_type', None)
    vote_type = POST_TYPE_MAP[vote_type]
    post_id = request.POST.get('post_id', None)
    value = int(request.POST.get('rating', None))

    if vote_type is None or post_id is None or value is None:
        return ajax_error(_('Vote type, post ID or rating is missing'))
    
    # Check the post that is voted on.
    post = Post.objects.get(pk=post_id)

    if post.author == user and vote_type == Vote.QUALITY:
        return ajax_error(_("You can't rate the quality of your own post."))

    if post.root.is_closed_survey():
        return ajax_error(_("The survey is now closed. It is not possible to vote in this thread anymore."))

    with transaction.atomic():
        msg, warning = perform_rating(post=post, user=user, vote_type=vote_type, value=value)

    return ajax_warning(msg) if warning else ajax_success(msg)


@ajax_error_wrapper
def get_user_ratings(request):
    "Get user ratings for posts"

    user = request.user
    post_ids = request.POST.get('post_ids', None)

    # Get the posts and the associated votes (do not return authors, just check if vote author is request.user)
    posts = Post.objects.filter(pk__in=post_ids).only('pk')

    results = []

    for post in posts:
        votes = Vote.objects.filter(post_id__in=posts_pks)
        votes_data = [{ 'author_is_me': vote.author == user, 'type': vote.type, 'rating': vote.value } for vote in votes]
        results.append({'post_pk': post.pk, 'votes': votes_data})

    return json_response({'status': 'success', 'results': results})


@ajax_error_wrapper
def cancel_ratings(request):
    "Cancel ratings for posts"

    user = request.user
    post_id = request.POST.get('post_id', None)

    votes = Vote.objects.filter(author=user, post_id=post_id, type__in=[Vote.CUSTOM_1, Vote.CUSTOM_2, Vote.CUSTOM_3, Vote.CUSTOM_4, Vote.CUSTOM_5, Vote.CUSTOM_6, Vote.CUSTOM_7, Vote.CUSTOM_8, Vote.CUSTOM_9, Vote.CUSTOM_10])
    votes.delete()

    return ajax_success(_('Votes cancelled'))


@ajax_error_wrapper
def survey_vote_handler(request):
    "Handles all rating on posts"

    user = request.user
    vote_type = request.POST.get('vote_type', None)
    
    if vote_type in POST_TYPE_MAP:
        ajax_error(_("Wrong survey vote type."))

    post_id = request.POST.get('post_id', None)
    value = int(request.POST.get('rating', None))

    if vote_type is None or post_id is None or value is None:
        return ajax_error(_('Vote type, post ID or rating is missing'))
    
    # Check the post that is voted on.
    post = Post.objects.get(pk=post_id)

    if post.root.is_closed_survey():
        return ajax_error(_("The survey is now closed. It is not possible to vote in this thread anymore."))

    with transaction.atomic():
        msg, warning = perform_survey_vote(post=post, user=user, vote_type=vote_type, value=value)

    return ajax_warning(msg) if warning else ajax_success(msg)


