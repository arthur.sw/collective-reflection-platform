from django.core.management import call_command
from django.conf import settings
from django.db import connection, transaction
from django.db.models.loading import get_app
from StringIO import StringIO
from django.core.management.base import BaseCommand, CommandError
import os, logging
from optparse import make_option
# from django.utils.timezone import datetime #important if using timezones
# from django.utils import timezone

from datetime import datetime
from django.utils.timezone import utc

logger = logging.getLogger(__name__)

class Command(BaseCommand):
    help = 'Performs actions on surveys'

    option_list = BaseCommand.option_list + ()

    def handle(self, *args, **options):

        close_surveys()

def close_surveys():
    from biostar.apps.users.models import User
    from biostar.apps.posts.models import Post, Survey
    from biostar.surveys import close_survey
    from biostar.apps.util import html

    print('close_surveys')

    # robot = User.objects.get(name='bot')
    
    now = datetime.utcnow()
    surveys = Survey.objects.filter(close_date__lt=now)

    for survey in surveys:
        # close_survey.delay(post=survey.post, user=robot)

        survey.post.status = Post.CLOSED
        survey.post.save()
        
        # content = html.render(name="messages/close_survey.html", post=survey.post)
        # comment = Post(content=content, type=Post.COMMENT, parent=survey.post, author=robot)
        # comment.save()

        # do not delete surveys: we need the close date when displaying surveys
        # survey.delete()

