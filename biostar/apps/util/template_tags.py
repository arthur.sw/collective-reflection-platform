import os.path
import json
import re
import numbers
from jsonschema import validate


from django.template.loader import render_to_string, TemplateDoesNotExist
from django.template import Context, Template
from django.contrib import messages

# statistics converter
def statistics(text, post, start, end, args):
    from biostar.apps.posts.models import Post

    # find the title in args
    title = next(( arg.replace('title=', '') for arg in args if arg.startswith('title=')), None )

    # find the vote types to show
    quality = 'quality' in args
    level_of_agreement = 'level_of_agreement' in args
    relevance = 'relevance' in args
    votes = 'votes' in args
    
    # auto if 'auto' in args or if nothing in args
    # if auto: set the vote types to show according to the post vote types
    auto = 'auto' in args or not quality and not level_of_agreement and not relevance and not votes

    # if in auto mode, override the vote types according to the post vote types
    if post and auto:
        # when submitting a post, post.vote_types is a string since it comes from a form: https://stackoverflow.com/questions/4187185/how-can-i-check-if-my-python-object-is-a-number
        vote_types = int(post.vote_types)
        votes = vote_types == Post.UP or vote_types == Post.UP_DOWN
        quality = not votes
        level_of_agreement = vote_types == Post.QUALITY_LOA or vote_types == Post.QUALITY_LOA_RELEVANCE
        relevance = vote_types == Post.QUALITY_LOA_RELEVANCE

    # find the active vote type (the one which will be displayed by default)
    active = next(( arg.replace('active=', '') for arg in args if arg.startswith('active=')), 'level_of_agreement' if level_of_agreement else 'quality' if quality else 'relevance' if relevance else 'votes' )

    multiple_types = quality + level_of_agreement + relevance + votes > 1

    # render the template
    template = render_to_string("server_tags/template_statistics.html", {
        'title': title,
        'quality': quality,
        'level_of_agreement': level_of_agreement,
        'relevance': relevance,
        'votes': votes,
        'active': active,
        'multiple_types': multiple_types,
    })

    # replace the tag by the template in the text and return the result
    return (text[:start] + template + text[end:], len(template) - (end - start))

def pov(text, post, start, end, args):
    template = render_to_string("server_tags/template_pov.html")
    return (text[:start] + template + text[end:], len(template) - (end - start))

def neologism(text, post, start, end, args):
    template = render_to_string("server_tags/template_neologism.html")
    return (text[:start] + template + text[end:], len(template) - (end - start))


def remove_outdated_polls(post):
    from biostar.apps.posts.models import Post, Vote

    # remove all outdated PollQuestions
    questions_and_choices = Post.objects.filter(parent=post, type__in=Post.POLL_TYPES)

    for question_or_choice in questions_and_choices:
        
        votes = Vote.objects.filter(post=question_or_choice)
        for vote in votes:
            vote.delete()

        print('delete question_or_choice:', question_or_choice.pk)

        question_or_choice.delete()

    return

def create_poll_question(post, data, question):
    from biostar.apps.posts.models import Post

    # Create the question and choices

    question_type = Post.get_type_from_name(question['type'])
    
    # try:
    #     question = Post.objects.get(parent=post, type=question_type, title=data['title'])
    # except ObjectDoesNotExist, exc:

    question_post = Post(parent=post, type=question_type, title=question['title'], author=post.author, content=question['description'])
    question_post.save()

    print('created question:', question_post.pk)

    question['id'] = question_post.pk

    # WARNING: the question type "multiple choice" must be converted to "radio" for the template to work properly
    question['type'] = 'radio' if question_type == Post.POLL_MULTIPLE_CHOICE else question['type']

    # Add new choices
    for index, choice in enumerate(question['choices']):

        choice_post = Post(parent=question_post, type=Post.POLL_CHOICE, title=choice, author=post.author, content="")
        choice_post.save()

        print('created choice:', choice_post.pk)

        question['choices'][index] = {
            'id': choice_post.pk,
            'text': choice
        }

    return question_post


def poll(post, data, request):
    from biostar.apps.posts.models import Post

    #  - create questions (existing questions and choices have been deleted),
    #  - convert the format of data to the expected format for the poll template

    # The given data will have the following format:
    # 
    # data = {
    #     template: "poll",
    #     title: "",
    #     questions: [
    #         {
    #             title: "",
    #             description: "",
    #             type: "",
    #             choices: ["..."]
    #         }
    #     ]
    # }

    # The poll template requires this format:
    # WARNING: the question type "multiple choice" must be converted to "radio" for the template to work properly

    # context = {
    #     error_message: "",
    #     post: Post(),
    #     title: "",
    #     question_ids: "[0,1,2,3]",
    #     questions: [
    #         {
    #             id: 0,
    #             title: "",
    #             description: "",
    #             type: "",
    #             choices: [{
    #                     id: 0,
    #                     text: ""
    #                 }
    #             ]
    #         }
    #     ]
    # }
    #
    #
    
    schema = {
        "type" : "object",
        "properties" : {
            "template": {"type": "string"},
            "title": {"type": "string"},
            "question": {
                "type": "array",
                "items": {
                    "type": "object",
                    "properties": {
                        "title": {"type": "string"},
                        "description": {"type": "string"},
                        "type": {"type": "string", "enum" : ["checkbox", "multiple_choice", "text"]},
                        "choices": {"type": "array", "items": {"type": "string"} }
                    }
                }
            }
        },
    }
    try:
        validate(data, schema)
    except ValidationError as e:
        messages.warning(request, "The poll template is not properly formatted.")
        return

    question_ids = []
    

    for question in data['questions']:
        question_post = create_poll_question(post, data, question)
        question_ids.append(question_post.pk)

    data['post'] = post
    data['question_ids'] = json.dumps(question_ids)
    
    print('question_ids', question_ids)

    return


TEMPLATE_TAGS = { 
    'Statistics': statistics,
    'POV': pov,
    'Neologism': neologism,
    'poll': poll,
 }


TAG_PATTERN = r"{{(?P<tag>.+)}}"
TAG_RE = re.compile(TAG_PATTERN, re.MULTILINE)

TEMPLATE_PATTERN = r"\[\s*?(?P<tag>{\s*?\"template\".+})\s*?\]"
TEMPLATE_RE = re.compile(TEMPLATE_PATTERN, re.MULTILINE | re.DOTALL)

TEMPLATE_PLACEHOLDER_PATTERN = r"\[{\d+}\]"
TEMPLATE_PLACEHOLDER_RE = re.compile(TEMPLATE_PLACEHOLDER_PATTERN, re.MULTILINE)

def get_closing_parenthesis_index(str):
    stack = []
    pushChars, popChars = "<({[", ">)}]"
    for index, c in enumerate(str):
        if c in pushChars :
            stack.append(c)
        elif c in popChars :
            if not len(stack) :
                return None
            else :
                stackTop = stack.pop()
                balancingBracket = pushChars[popChars.index(c)]
                if stackTop != balancingBracket :
                    return None
                elif len(stack) == 0:
                    return index
        else:
            continue

    return None


def parse_templates(text, post, request=None):
    "Create a list of html templates from template tags"
    
    from biostar.apps.posts.models import Post

    templates = []

    remove_outdated_polls(post)

    # Find all occurence of {{ template}} tags
    matches = re.finditer(TEMPLATE_RE, text)
    offset = 0

    datas = []
    # For each occurence: replace the tag by the corresponding html
    for matchNum, match in enumerate(matches):
        
        # get the tag name and its arguments
        tag = match.group("tag")

        closing_parenthesis_index = get_closing_parenthesis_index(tag)
        tag = tag[:closing_parenthesis_index+1]
        
        try:
            data = json.loads(tag)
        except ValueError as e:
            if request:
                messages.warning(request, "An error occured while parsing template tag, make sure it corresponds to valid JSON.")
            continue

        rendered_template = None

        template_name = data["template"].lower().replace(" ", "_")
        data['template_name'] = template_name
        template_file_name = "server_tags/template_" + template_name + ".html"

        if template_name in TEMPLATE_TAGS:
            preprocessor = TEMPLATE_TAGS[template_name]
            preprocessor(post, data, request)

        try:
            rendered_template = render_to_string(template_file_name, data)
        except TemplateDoesNotExist as e:
            posts = Post.objects.filter(type=Post.TEMPLATE_ARTICLE, title=data["template"], status=Post.CERTIFIED)
            if posts:
                template_article = posts[0]
                template = Template(template_article.content)
                context = Context(data)
                rendered_template = template.render(context)
            else:
                messages.warning(request, "No template matches the given name.")
                
        if rendered_template:
            start = offset + match.start()
            end = offset + match.end()
            templates.append(rendered_template)
            placeholder = '[{' + str(matchNum) + '}]'
            text = text[:start] + placeholder + text[end:]
            offset += len(placeholder) - (end - start)

        datas.append(data)


    return (templates, text)

def integrate_templates(html, templates, post, request=None):
    
    # Find all occurence of [{123}] tags

    matches = re.finditer(TEMPLATE_PLACEHOLDER_RE, html)
    offset = 0

    # For each occurence: replace the placeholder by the corresponding template
    for matchNum, match in enumerate(matches):

        start = offset + match.start()
        end = offset + match.end()
        html = html[:start] + templates[matchNum] + html[end:]
        offset += len(templates[matchNum]) - (end - start)

    return html

# INCLUDE_POSTS_PATTERN = r"\[(?P<tag>{\s*\"post\".+})\]"
# INCLUDE_POSTS_RE = re.compile(INCLUDE_POSTS_PATTERN, re.MULTILINE)

# def render_dynamic_content(post):

#     text = post.content

#     # Find all occurence of {{ template}} tags
#     matches = re.finditer(INCLUDE_POSTS_PATTERN, text)
#     offset = 0

#     sections = [text]
#     lastSectionIndex = 0

#     # For each occurence: replace the tag by the corresponding html
#     for matchNum, match in enumerate(matches):
        
#         start = match.start()
#         end = match.end()
#         tag = match.group("tag")
#         data = json.loads(tag)

#         sections[len(sections)-1] = text[lastSectionIndex:start-1]
#         post_html = None
#         try:
#             child = Post.objects.get(pk=data['post_id'])
#             post_html = child.html
#         except ObjectDoesNotExist, exc:
#             post_html = ""
#         sections.append(post_html)
#         if end+1 < len(text):
#             sections.append(text[end+1:])
#         lastSectionIndex = end

#     # render the template
#     html = render_to_string("server_tags/dynamic_content.html", { 'post': post, 'sections': sections })

#     return html



# def parse_markdown(text, post, request=None):
#     "Convert template tags into html"
    
#     from biostar.apps.posts.models import Post

#     # Find all occurence of {{ template}} tags
#     matches = re.finditer(TAG_RE, text)
#     offset = 0

#     # For each occurence: replace the tag by the corresponding html
#     for matchNum, match in enumerate(matches):
#         matchNum = matchNum + 1
        
#         # get the tag name and its arguments
#         tag = match.group("tag")
#         args = tag.split('|')
        
#         # call the markdown-to-html converter corresponding to the tag
#         if len(args) > 0 and args[0] in TEMPLATE_TAGS:
#             converter = TEMPLATE_TAGS[args[0]]
#             text, local_offset = converter(text, post, offset + match.start(), offset + match.end(), args)
#             offset += local_offset

#     # Find all occurence of [{ "template": "template_name", "options": "json object" }] tags
#     matches = re.finditer(TEMPLATE_RE, text)
#     offset = 0
    
#     import pdb; pdb.set_trace()

#     datas = []
#     # For each occurence: replace the tag by the corresponding html
#     for matchNum, match in enumerate(matches):
#         matchNum = matchNum + 1
        
#         # get the tag name and its arguments
#         tag = match.group("tag")

#         closing_parenthesis_index = get_closing_parenthesis_index(tag)
#         tag = tag[:closing_parenthesis_index+1]
        
#         try:
#             data = json.loads(tag)
#         except ValueError as e:
#             if request:
#                 messages.warning(request, "An error occured while parsing template tag, make sure it corresponds to valid JSON.")
#             continue

#         rendered_template = None

#         template_name = data["template"].lower().replace(" ", "_")
#         data['template_name'] = template_name
#         template_file_name = "server_tags/" + template_name + ".html"

#         if template_name in TEMPLATE_TAGS:
#             preprocessor = TEMPLATE_TAGS[template_name]
#             preprocessor(post, data)

#         try:
#             rendered_template = render_to_string(template_file_name, data)
#         except TemplateDoesNotExist as e:
#             posts = Post.objects.filter(type=Post.TEMPLATE_ARTICLE, title=data["template"], status=Post.CERTIFIED)
#             if posts:
#                 template_article = posts[0]
#                 template = Template(template_article.content)
#                 context = Context(data)
#                 rendered_template = template.render(context)
#             else:
#                 messages.warning(request, "No template matches the given name.")
                
#         if rendered_template:
#             start = offset + match.start()
#             end = offset + match.end()
#             text = text[:start] + rendered_template + text[end:]
#             offset += len(rendered_template) - (end - start)

#         datas.append(data)

#     remove_outdated_polls(post, datas)

#     return text