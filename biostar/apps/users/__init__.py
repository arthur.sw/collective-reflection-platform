from .models import User, Profile

def create_robot():
	try:
		robot = User.objects.get_or_create(name='bot', email='bot@platform.co', type=User.ADMIN, status=User.TRUSTED, flair='The kind robot of the platform')
		profile = Profile.objects.get_or_create(user=robot[0])
	except:
		print("Database not initialized yet")
		pass

create_robot()