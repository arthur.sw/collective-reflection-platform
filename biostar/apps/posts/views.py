# Create your views here.
from django.shortcuts import render_to_response
from django.views.generic import TemplateView, DetailView, ListView, FormView, UpdateView
from .models import Post, Vote, Survey
from biostar.apps.users.models import User
from django import forms
from django.core.urlresolvers import reverse
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field, Fieldset, Div, Submit, ButtonHolder
from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpRequest, HttpResponse, HttpResponseNotFound
from django.contrib import messages
from . import auth
from braces.views import LoginRequiredMixin
from datetime import datetime, timedelta
from django.utils.timezone import utc
from django.core.exceptions import ObjectDoesNotExist
from django.conf import settings
from biostar.const import OrderedDict
from django.core.exceptions import ValidationError
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from biostar.apps.util import html
from django.shortcuts import get_object_or_404, redirect

import random
import re
import pytz
import logging
import json
import markdown2

import langdetect
from django.template.loader import render_to_string

def english_only(text):
    try:
        text.decode('ascii')
    except Exception:
        raise ValidationError('Title may only contain plain text (ASCII) characters')


def valid_language(text):
    supported_languages = settings.LANGUAGE_DETECTION
    if supported_languages:
        lang = langdetect.detect(text)
        if lang not in supported_languages:
            raise ValidationError(
                    'Language "{0}" is not one of the supported languages {1}!'.format(lang, supported_languages))

logger = logging.getLogger(__name__)


def valid_title(text):
    "Validates form input for tags"
    text = text.strip()
    if not text:
        raise ValidationError('Please enter a title')

    if len(text) < 3:
        raise ValidationError('The title is too short')


def valid_tag(text):
    "Validates form input for tags"
    text = text.strip()
    if not text:
        raise ValidationError('Please enter at least one tag')
    if len(text) > 50:
        raise ValidationError('The tag line is too long (50 characters max)')
    words = text.split(",")
    if len(words) > 5:
        raise ValidationError('Please enter no more than 5 tags')

class PagedownWidget(forms.Textarea):
    TEMPLATE = "pagedown_widget.html"

    def render(self, name, value, attrs=None):
        value = value or ''
        rows = attrs.get('rows', 15)
        klass = attrs.get('class', '')
        params = dict(value=value, rows=rows, klass=klass)
        return render_to_string(self.TEMPLATE, params)


class LongForm(forms.Form):
    FIELDS = "title content post_type vote_types tag_val enable_deadline close_date close_time time_zone".split()

    POST_CHOICES = [(Post.FORUM, "Discussion"),
                    (Post.QUESTION, "Question"),
                    (Post.DEBATE, "Debate"),
                    (Post.SURVEY, "Survey"),
                    (Post.ARTICLE, "Article"),
                    (Post.TEMPLATE_ARTICLE, "Template"),
                    (Post.META, "Meta")]

    title = forms.CharField(
        label="Title",
        max_length=200, min_length=3, validators=[valid_title, english_only],
        help_text="Descriptive titles promote better answers.", widget=forms.TextInput(attrs={ 'autocomplete': 'off' }))

    post_type = forms.ChoiceField(
        label="Type",
        choices=POST_CHOICES, help_text="Select a post type: Discussion, Question, Debate, Survey, Article, etc.")

    vote_types = forms.ChoiceField(
        label="Vote type",
        choices=Post.VOTE_TYPES, help_text="(Experimental feature) Select how others will rate the post: vote up / down, quality, quality + level of agreement, etc.")

    tag_val = forms.CharField(
        label="Tags",
        required=True, validators=[valid_tag],
        help_text="Choose one or more tags to match the topic. To create a new tag just type it in and press ENTER.",
    )

    enable_deadline = forms.BooleanField(required=False, help_text="It is not possible to participate to surveys after the deadline.", initial=True)

    close_date = forms.DateField(widget=forms.DateInput(attrs={ 'autocomplete': 'off' }), help_text="The deadline of the survey.")
    close_time = forms.TimeField(widget=forms.TimeInput(attrs={ 'autocomplete': 'off' }), input_formats=['%H:%M:%S','%H:%M','%I:%M %p'], help_text="The time at which the survey closes.")
    time_zone = forms.CharField(label="Timezone", max_length=200, min_length=3)

    content = forms.CharField(widget=PagedownWidget, validators=[valid_language],
                              min_length=50, max_length=15000,
                              label="Body")

    def __init__(self, *args, **kwargs):
        super(LongForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = "post-form"
        self.helper.layout = Layout(
            Fieldset(
                'Post',
                Field('title'),
                Field('post_type'),
                Field('enable_deadline'),
                Field('close_date'),
                Field('close_time'),
                Field('time_zone'),
                Field('content'),
                Field('tag_val'),
                Field('vote_types'),
            ),
            ButtonHolder(
                Submit('submit', 'Submit')
            )
        )
        self.fields['time_zone'].widget = forms.HiddenInput()

class ShortForm(forms.Form):
    FIELDS = ["content", "post_type"]

    CHOICES = ((Post.ANSWER, 'Neutral',), (Post.ARGUMENT_FOR, 'Argument for',), (Post.ARGUMENT_AGAINST, 'Argument against',))

    content = forms.CharField(widget=PagedownWidget, min_length=10, max_length=5000,)
    post_type = forms.ChoiceField(widget=forms.RadioSelect, choices=CHOICES, required=False)

    def __init__(self, *args, **kwargs):
        debate = 'debate' in kwargs and kwargs['debate']
        if 'debate' in kwargs:
            del kwargs['debate']
        super(ShortForm, self).__init__(*args, **kwargs)
        self.initial['post_type'] = Post.ANSWER
        self.helper = FormHelper()
        fieldset = Fieldset('Post', 'content', 'post_type') if debate else Fieldset('Post', 'content',)
        self.helper.layout = Layout(
            fieldset,
            ButtonHolder(
                Submit('submit', 'Submit')
            )
        )

        if not debate:
            self.fields['post_type'].widget = forms.HiddenInput()


def parse_tags(category, tag_val):
    pass


@login_required
@csrf_exempt
def external_post_handler(request):
    "This is used to pre-populate a new form submission"
    import hmac

    user = request.user
    home = reverse("home")
    name = request.REQUEST.get("name")

    if not name:
        messages.error(request, "Incorrect request. The name parameter is missing")
        return HttpResponseRedirect(home)

    try:
        secret = dict(settings.EXTERNAL_AUTH).get(name)
    except Exception, exc:
        logger.error(exc)
        messages.error(request, "Incorrect EXTERNAL_AUTH settings, internal exception")
        return HttpResponseRedirect(home)

    if not secret:
        messages.error(request, "Incorrect EXTERNAL_AUTH, no KEY found for this name")
        return HttpResponseRedirect(home)

    content = request.REQUEST.get("content")
    submit = request.REQUEST.get("action")
    digest1 = request.REQUEST.get("digest")
    digest2 = hmac.new(secret, content).hexdigest()

    if digest1 != digest2:
        messages.error(request, "digests does not match")
        return HttpResponseRedirect(home)

    # auto submit the post
    if submit:
        post = Post(author=user, type=Post.QUESTION)
        for field in settings.EXTERNAL_SESSION_FIELDS:
            setattr(post, field, request.REQUEST.get(field, ''))
        post.save()
        post.add_tags(post.tag_val)
        return HttpResponseRedirect(reverse("post-details", kwargs=dict(pk=post.id)))

    # pre populate the form
    sess = request.session
    sess[settings.EXTERNAL_SESSION_KEY] = dict()
    for field in settings.EXTERNAL_SESSION_FIELDS:
        sess[settings.EXTERNAL_SESSION_KEY][field] = request.REQUEST.get(field, '')

    return HttpResponseRedirect(reverse("new-post"))


def get_augmented_link(request):

    link_content = request.GET.get('link_content', None)
    pid = request.GET.get('post_id', None)

    try:
        post = Post.objects.get(pk=pid)
    except ObjectDoesNotExist, exc:
        messages.error(request, "The post does not exist. Perhaps it was deleted")
        return HttpResponseNotFound('<h1>Post not found</h1>')

    # return HttpResponse("Here's the text of the Web page.")
    return render(request, "server_tags/augmented_link.html", {'link_content': link_content, 'post': post})

class NewPost(LoginRequiredMixin, FormView):
    form_class = LongForm
    template_name = "post_edit.html"

    def get(self, request, *args, **kwargs):
        initial = dict()

        # Attempt to prefill from GET parameters
        for key in "title tag_val content".split():
            value = request.GET.get(key)
            if value:
                initial[key] = value


        # Attempt to prefill from external session
        sess = request.session
        if settings.EXTERNAL_SESSION_KEY in sess:
            for field in settings.EXTERNAL_SESSION_FIELDS:
                initial[field] = sess[settings.EXTERNAL_SESSION_KEY].get(field)
            del sess[settings.EXTERNAL_SESSION_KEY]

        form = self.form_class(initial=initial)
        return render(request, self.template_name, {'form': form})


    def post(self, request, *args, **kwargs):
        # Validating the form.
        form = self.form_class(request.POST)
        
        if not form.is_valid():
            return render(request, self.template_name, {'form': form})

        # Valid forms start here.
        data = form.cleaned_data.get

        title = data('title')
        content = data('content')
        post_type = int(data('post_type'))
        vote_types = int(data('vote_types'))
        tag_val = data('tag_val')
        enable_deadline = data('enable_deadline')
        close_date = data('close_date')
        close_time = data('close_time')
        time_zone = data('time_zone')

        site = Post.objects.get_site(request)

        post = Post(
            title=title, content=content, tag_val=tag_val,
            author=request.user, type=post_type, vote_types=vote_types, site=site
        )
        post.save(request=request)

        # If type is survey:
        # - create a survey ; a celery task will regularly check all surveys to close them if they expire
        # - add a comment explaining that it is a survey and when it closes
        if post_type == Post.SURVEY and enable_deadline:

            # survey_close_date = datetime(close_date.year, close_date.month, close_date.day, 
            #     hour=close_time.hour, minute=close_time.minute, second=close_time.second,
            #     tzinfo=pytz.timezone(time_zone)
            # )
            survey_close_date = pytz.timezone(time_zone).localize(datetime(close_date.year, close_date.month, close_date.day, close_time.hour, close_time.minute))

            survey = Survey(post=post, close_date=survey_close_date)
            survey.save()

            # robot = User.objects.get(name='bot')

            # content = html.render(name="messages/survey.html", close_date=close_date, close_time=close_time)
            # comment = Post(content=content, type=Post.COMMENT, parent=post, author=robot)
            # comment.save()


        # Triggers a new post save.
        post.add_tags(post.tag_val)

        messages.success(request, "%s created" % post.get_type_display())
        return HttpResponseRedirect(post.get_absolute_url())


class NewAnswer(LoginRequiredMixin, FormView):
    """
    Creates a new post.
    """
    form_class = ShortForm
    template_name = "post_edit.html"
    type_map = dict(answer=Post.ANSWER, comment=Post.COMMENT)
    post_type = None

    def get(self, request, *args, **kwargs):
        initial = dict(content='', post_type=Post.ANSWER)

        # The parent id.
        pid = int(self.kwargs['pid'])
        # form_class = ShortForm if pid else LongForm
        form = self.form_class(initial=initial)

        return render(request, self.template_name, {'form': form, 'pre': False})

    def post(self, request, *args, **kwargs):

        pid = int(self.kwargs['pid'])

        # Find the parent.
        try:
            parent = Post.objects.get(pk=pid)
        except ObjectDoesNotExist, exc:
            messages.error(request, "The post does not exist. Perhaps it was deleted")
            return HttpResponseRedirect("/")

        if parent.root.type == Post.EDIT_HISTORY:
            messages.error(request, "This thread is generated automatically and cannot be edited.")
            return HttpResponseRedirect(parent.get_absolute_url())

        # Validating the form.
        form = self.form_class(request.POST)

        if not form.is_valid():
            return render(request, self.template_name, {'form': form})

        # Valid forms start here.
        data = form.cleaned_data.get

        # Figure out the right type for this new post
        post_type = self.type_map.get(self.post_type)
        
        # if it is an answer, check if it is ANSWER, ARGUMENT_FOR or ARGUMENT_AGAINST
        if post_type == Post.ANSWER:
            post_type = data('post_type')

        site = Post.objects.get_site(request)

        # Create a new post.
        post = Post(
            title=parent.title, content=data('content'), author=request.user, type=post_type,
            parent=parent, site=site, vote_types=parent.vote_types
        )

        messages.success(request, "%s created" % post.get_type_display())
        post.save(request=request)

        return HttpResponseRedirect(post.get_absolute_url())


class EditPost(LoginRequiredMixin, FormView):
    """
    Edits an existing post.
    """

    # The template_name attribute must be specified in the calling apps.
    template_name = "post_edit.html"
    form_class = LongForm

    def get(self, request, *args, **kwargs):
        initial = {}

        pk = int(self.kwargs['pk'])
        post = Post.objects.get(pk=pk)
        post = auth.post_permissions(request=request, post=post)

        # Check and exit if not a valid edit.
        if not post.is_editable:
            messages.error(request, "This user may not modify the post")
            return HttpResponseRedirect(reverse("home"))

        if post.root.type == Post.EDIT_HISTORY:
            messages.error(request, "This thread is generated automatically and cannot be edited.")
            return HttpResponseRedirect(post.get_absolute_url())

        initial = dict(title=post.title, content=post.content, post_type=post.type, tag_val=post.tag_val, vote_types=post.vote_types)

        if post.type == Post.SURVEY:
            try:
                survey = Survey.objects.get(post=post)
                initial['close_date'] = survey.close_date.date()
                initial['close_time'] = survey.close_date.time()
            except ObjectDoesNotExist, exc:
                pass

        # Disable rich editing for preformatted posts
        pre = 'class="preformatted"' in post.content

        form_class = LongForm if post.is_toplevel else ShortForm
        form = form_class(initial=initial)

        return render(request, self.template_name, {'form': form, 'pre': pre})

    def post(self, request, *args, **kwargs):

        pk = int(self.kwargs['pk'])
        post = Post.objects.get(pk=pk)
        original_content = post.content
        original_html = post.html
        post = auth.post_permissions(request=request, post=post)

        # For historical reasons we had posts with iframes
        # these cannot be edited because the content would be lost in the front end
        if "<iframe" in post.content:
            messages.error(request, "This post is not editable because of an iframe! Contact if you must edit it")
            return HttpResponseRedirect(post.get_absolute_url())

        # Check and exit if not a valid edit.
        if not post.is_editable:
            messages.error(request, "This user may not modify the post")
            return HttpResponseRedirect(post.get_absolute_url())

        if post.root.type == Post.EDIT_HISTORY:
            messages.error(request, "This thread is generated automatically and cannot be edited.")
            return HttpResponseRedirect(post.get_absolute_url())

        # Posts with a parent are not toplevel
        form_class = LongForm if post.is_toplevel else ShortForm

        form = form_class(request.POST)
        if not form.is_valid():
            # Invalid form submission.
            return render(request, self.template_name, {'form': form})

        # Valid forms start here.
        data = form.cleaned_data

        # Update vote_types on all children if vote_types changed
        vote_types = data.get('vote_types', Post.QUALITY)
        if vote_types != post.vote_types:
            for child in post.children.all():
                # Ignore self since posts are child of themselves by default
                if child != post and child.type != Post.COMMENT and child.type not in Post.POLL_TYPES:
                    child.vote_types = vote_types
                    child.save()

        # Set the form attributes.
        for field in form_class.FIELDS:
            setattr(post, field, data[field])

        # TODO: fix this oversight!
        post_type = data.get('post_type', post.type)
        post.type = int( post_type if post_type != u'' else post.type )

        # This is needed to validate some fields.
        post.save(parse_html=False)

        if post.is_toplevel:
            post.add_tags(post.tag_val)

        # Update the last editing user.
        post.lastedit_user = request.user

        # Only editing by author bumps the post.
        if request.user == post.author:
            post.lastedit_date = datetime.utcnow().replace(tzinfo=utc)

        # If the the content was edited: update edit history
        edit_history = None

        if original_content != post.content:
            
            

            # Find meta post about edit history or create one
            edit_history = post.edit_history

            robot = User.objects.get(name='bot')

            if edit_history is None:

                print("create edit history")

                # Create a meta thread to publish the edit history
                header = 'Edit history of the post [' + post.title + '](' + post.get_absolute_url() + ') \n\n'
                header += '***Original content***\n\n'

                edit_history_content = header + original_content

                header_html = markdown2.markdown(header)

                edit_history_html = header_html + original_html

                edit_history = Post(
                    title='Edit history of "' + post.title + '"', content=edit_history_content, html=edit_history_html, tag_val='meta',
                    author=robot, type=Post.EDIT_HISTORY, status=Post.CLOSED, site=post.site
                )
                edit_history.save(parse_html=False)
                post.edit_history = edit_history
            
            print("update edit history")

            # Create a meta post correponding to the current edit
            # TODO: check how stackoverflow rewards reputation on posts which have multiple authors.
            header = 'Edited by [' + request.user.name + '](' + request.user.get_absolute_url() + '):\n\n'
            
            edit_content = header + post.content
            header_html = markdown2.markdown(header)
            edit_html = header_html + post.html

            edit = Post(
                title=edit_history.title, content=edit_content, html=edit_html, tag_val='meta',
                author=robot, type=Post.EDIT, parent=edit_history, site=post.site
            )
            edit.save(parse_html=False)

        print("parse html and resave post:")
        
        post.save(request=request)

        print("post saved")

        if edit_history:

            # Warn moderators if there was 5 edits in the last 10 minutes:
            # TODO: put variables in const

            edit_history_children = edit_history.children.all()

            if len(edit_history_children) > 5:

                edits = edit_history_children.order_by("-lastedit_date")[:5]

                if post.lastedit_date - edits[0].lastedit_date > timedelta(minutes=10):

                    # Send a message to 5 random moderators 

                    admin_ids = [ u[0] for u in User.objects.filter(is_admin=True).values_list("id") ]

                    random.shuffle(admin_ids)

                    admin_ids = admin_ids[:5]

                    for admin_id in admin_ids:
                        user = User.objects.get(pk=admin_id)

                        title = "Frequent edits warning"
                        content = html.render(name="messages/frequent_edits.html", user=user)
                        body = MessageBody.objects.create(author=robot, subject=title,
                                                          text=content, sent_at=now())
                        message = Message(user=user, body=body, sent_at=body.sent_at)
                        message.save()

        post_type = int(post_type) if post_type and post_type != u'' else None

        # Update survey if close_date or close_time were edited
        if post_type == Post.SURVEY:
            close_date = data.get('close_date', None)
            close_time = data.get('close_time', None)
            time_zone = data.get('time_zone', 'UTC')

            if close_date and close_time:
                try:
                    survey = Survey.objects.get(post=post)
                    if close_date != survey.close_date.date() or close_time != survey.close_date.time():
                        survey.close_date = pytz.timezone(time_zone).localize(datetime(close_date.year, close_date.month, close_date.day, close_time.hour, close_time.minute))
                        # survey.close_date = datetime(close_date.year, close_date.month, close_date.day, 
                        #     hour=close_time.hour, minute=close_time.minute, second=close_time.second,
                        #     tzinfo=pytz.timezone(time_zone)
                        # )

                        survey.save()
                except ObjectDoesNotExist, exc:
                    pass


        messages.success(request, "Post updated")

        return HttpResponseRedirect(post.get_absolute_url())

    def get_success_url(self):
        return reverse("user_details", kwargs=dict(pk=self.kwargs['pk']))

# Submit participation to a poll

def poll_submit(request, pk, question_ids):

    if not request.user.is_authenticated():
        messages.error(request, "You must be authenticated to submit answers.")
        return redirect(request.META['HTTP_REFERER']) 

    post = get_object_or_404(Post, pk=pk)

    try:

        # Get all questions and vote for the corresponding choices
        qids = json.loads(question_ids)

    except ValueError as e:
        
        messages.error(request, "The questions could not be retrieved.")
        return HttpResponseRedirect(post.get_absolute_url())

    else:

        for question_id in qids:

            try:
                question = Post.objects.get(pk=question_id)
            except ObjectDoesNotExist, exc:
                messages.error(request, "The question " + str(question_id) + " does not exist. Perhaps it was deleted.")
                return HttpResponseRedirect(post.get_absolute_url())
            else:
                
                # Vote for the choices

                # Check if votes exists: delete them if they exist
                votes = Vote.objects.filter(author=request.user, post__in=question.children.all(), type=Vote.POLL)
                for vote in votes:
                    vote.delete()

                # Create a "validation" vote for the question: means that the user submited an answer

                try:
                    validation_vote = Vote.objects.get(author=request.user, post=question, type=Vote.POLL)
                except ObjectDoesNotExist, exc:
                    validation_vote = Vote.objects.create(author=request.user, post=question, type=Vote.POLL, value=1)
                    validation_vote.save()

                # Create the vote(s):

                # if radio button: just create vote for the selected one
                if question.type == Post.POLL_MULTIPLE_CHOICE:

                    # get the corresponding choice
                    choice_id = request.POST['question_' + str(question.pk)]
                    
                    try:
                        choice = Post.objects.get(pk=choice_id)
                    except ObjectDoesNotExist, exc:
                        messages.error(request, "The choice does not exist. Perhaps it was deleted")
                        return HttpResponseRedirect(post.get_absolute_url())

                    
                    # create the new vote
                    vote = Vote.objects.create(author=request.user, post=choice, type=Vote.POLL, value=1)
                    vote.save()

                # if checkbox: create vote for each choice
                elif question.type == Post.POLL_CHECKBOX:

                    # create vote for each choice

                    # get a list of all checked checkboxes (unchecked checkboxes are not returned)
                    values = request.POST.getlist('question_' + str(question.pk))

                    # for each choice: create vote with value == 1 if the vote is in the list, 0 otherwise 
                    for choice in question.children.all():

                        choice_id = str(choice.pk)

                        value = 1 if choice_id in values else 0

                        vote = Vote.objects.create(author=request.user, post=choice, type=Vote.POLL, value=value)
                        vote.save()

        # Always return an HttpResponseRedirect after successfully dealing
        # with POST data. This prevents data from being posted twice if a
        # user hits the Back button.
        return HttpResponseRedirect(post.get_absolute_url())
