from __future__ import print_function, unicode_literals, absolute_import, division
import logging, datetime, string
from django.db import models
from django.conf import settings
from django.contrib import admin
from django.contrib.sites.models import Site

from django.utils.timezone import utc
from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse
import bleach
from django.db.models import Q, F
from django.core.exceptions import ObjectDoesNotExist
from biostar import const
from biostar.apps.util import html
from biostar.apps import util
import re

# HTML sanitization parameters.

logger = logging.getLogger(__name__)

def now():
    return datetime.datetime.utcnow().replace(tzinfo=utc)

class Tag(models.Model):
    name = models.TextField(max_length=50, db_index=True)
    count = models.IntegerField(default=0)
    
    # What site does the tag belong to.
    site = models.ForeignKey(Site, null=True, db_index=True)

    @staticmethod
    def fixcase(name):
        return name.upper() if len(name) == 1 else name.lower()

    @staticmethod
    def update_counts(sender, instance, action, pk_set, *args, **kwargs):
        "Applies tag count updates upon post changes"

        if action == 'post_add':
            Tag.objects.filter(pk__in=pk_set).update(count=F('count') + 1)

        if action == 'post_remove':
            Tag.objects.filter(pk__in=pk_set).update(count=F('count') - 1)

        if action == 'pre_clear':
            instance.tag_set.all().update(count=F('count') - 1)

    def __unicode__(self):
        return self.name

class TagAdmin(admin.ModelAdmin):
    list_display = ('name', 'count')
    search_fields = ['name']


admin.site.register(Tag, TagAdmin)

class PostManager(models.Manager):

    def my_bookmarks(self, user, request=None):
        site = self.get_site(request)
        if site:
            query = self.filter(votes__author=user, votes__type=Vote.BOOKMARK, site=site)
        else:
            query = self.filter(votes__author=user, votes__type=Vote.BOOKMARK)
        query = query.select_related("root", "author", "lastedit_user")
        query = query.prefetch_related("tag_set")
        return query

    def my_posts(self, target, user, request=None):

        site = self.get_site(request)

        # Show all posts for moderators or targets
        if user.is_moderator or user == target:
            if site is None:
                query = self.filter(author=target)
            else:
                query = self.filter(author=target, site=site)
        else:
            if site is None:
                query = self.filter(author=target).exclude(status=Post.DELETED)
            else:
                query = self.filter(author=target, site=site).exclude(status=Post.DELETED)

        query = query.select_related("root", "author", "lastedit_user")
        query = query.prefetch_related("tag_set")
        query = query.order_by("-creation_date")
        return query

    def fixcase(self, text):
        return text.upper() if len(text) == 1 else text.lower()

    def tag_search(self, text, request=None):
        "Performs a query by one or more , separated tags"
        include, exclude = [], []
        # Split the given tags on ',' and '+'.
        terms = text.split(',') if ',' in text else text.split('+')
        for term in terms:
            term = term.strip()
            if term.endswith("!"):
                exclude.append(self.fixcase(term[:-1]))
            else:
                include.append(self.fixcase(term))

        site = self.get_site(request)

        if include:
            query = self.filter(type__in=Post.TOP_LEVEL, tag_set__name__in=include, site=site).exclude(
                tag_set__name__in=exclude)
        else:
            query = self.filter(type__in=Post.TOP_LEVEL, site=site).exclude(tag_set__name__in=exclude)

        query = query.filter(status=Post.OPEN).exclude(type=Post.EDIT_HISTORY)

        # Remove fields that are not used.
        query = query.defer('content', 'html')

        # Get the tags.
        query = query.select_related("root", "author", "lastedit_user").prefetch_related("tag_set").distinct()

        return query

    def get_thread(self, root, user, sort_order="types"):
        # Populate the object to build a tree that contains all posts in the thread.
        is_moderator = user.is_authenticated() and user.is_moderator
        order_by = ["type", "-has_accepted", "-quality", "-vote_count", "creation_date"]
        if sort_order == "votes":
            order_by = ["-quality", "-vote_count", "type", "-has_accepted", "creation_date"]
        elif sort_order == "quality":
            order_by = ["-quality", "type", "-has_accepted", "creation_date"]
        elif sort_order == "relevance":
            order_by = ["-relevance", "type", "-has_accepted", "creation_date"]
        elif sort_order == "level_of_agreement":
            order_by = ["-level_of_agreement", "type", "-has_accepted", "creation_date"]
        elif sort_order == "creation_dates":
            order_by = ["creation_date", "-quality", "-vote_count", "type", "-has_accepted"]
        
        cond = Q(root=root) & ~Q(pk=root.pk) & ~Q(type__in=Post.POLL_TYPES)

        if not is_moderator:
            cond &= ~Q(status=Post.DELETED)

        query = self.filter(cond).select_related("root", "author", "lastedit_user").order_by(*order_by)

        return query

    def top_level(self, user, request=None):
        "Returns posts based on a user type"
        is_moderator = user.is_authenticated() and user.is_moderator
        
        site = self.get_site(request)

        if is_moderator:
            query = self.filter(type__in=Post.TOP_LEVEL, site=site).exclude(type=Post.EDIT_HISTORY)
        else:
            query = self.filter(type__in=Post.TOP_LEVEL, site=site).exclude(status=Post.DELETED).exclude(type=Post.EDIT_HISTORY)

        return query.select_related("root", "author", "lastedit_user").prefetch_related("tag_set").defer("content", "html")

    def get_site(self, request):
        site = None
        if request:

            domain = request.subdomain

            if not domain and request.META.has_key('HTTP_HOST'):
                http_host = request.META['HTTP_HOST']
                domain, n_replacements = re.subn(r"\.[a-z-]+\.[a-z]+$", '', http_host)
                if n_replacements == 0:
                    domain = ''
            if domain and len(domain) > 0:
                try:
                    site = Site.objects.get(domain=domain)
                except ObjectDoesNotExist:
                    site = None
        return site

class Post(models.Model):
    "Represents a post in Biostar"
    
    def save_without_signals(self):
        """
        This allows for updating the model from code running inside post_save()
        signals without going into an infinite loop:
        """
        self._disable_signals = True
        self.save()
        self._disable_signals = False

    objects = PostManager()

    # Post statuses.
    PENDING, OPEN, CLOSED, DELETED, CERTIFIED = range(5)
    STATUS_CHOICES = [(PENDING, "Pending"), (OPEN, "Open"), (CLOSED, "Closed"), (DELETED, "Deleted"), (CERTIFIED, "Certified")]

    # Vote types
    # Experimental feature for research purpose only
    # When creating a post, the user chooses how this post will be rated
    UP, UP_DOWN, QUALITY, QUALITY_LOA, QUALITY_LOA_RELEVANCE, NONE = range(6)
    VOTE_TYPES = [(UP, "Up"), (UP_DOWN, "Up / Down"), (QUALITY, "Quality"), (QUALITY_LOA, "Quality, Level of agreement"), (QUALITY_LOA_RELEVANCE, "Quality, Level of agreement, Relevance"), (NONE, "None")]

    # Question types. Answers should be listed before comments.
    QUESTION, ARGUMENT_FOR, ARGUMENT_AGAINST, ANSWER, DEBATE, SURVEY, JOB, FORUM, PAGE, BLOG, COMMENT, DATA, TUTORIAL, BOARD, TOOL, NEWS, META, EDIT_HISTORY, EDIT, ARTICLE, TEMPLATE_ARTICLE, POLL_MULTIPLE_CHOICE, POLL_CHECKBOX, POLL_CHOICE = range(24)

    TYPE_CHOICES = [
        (QUESTION, "Question"), (ANSWER, "Answer"), (ARGUMENT_FOR, "Argument For"), (ARGUMENT_AGAINST, "Argument Against"), (COMMENT, "Comment"),
        (DEBATE, "Debate"), (SURVEY, "Survey"), (JOB, "Job"), (FORUM, "Forum"), (TUTORIAL, "Tutorial"), (META, "Meta"), (EDIT_HISTORY, "Edit History"), (EDIT, "Edit"),
        (DATA, "Data"), (PAGE, "Page"), (TOOL, "Tool"), (NEWS, "News"),
        (BLOG, "Blog"), (BOARD, "Bulletin Board"), (ARTICLE, "Article"), (TEMPLATE_ARTICLE, "Template Article"),
        (POLL_MULTIPLE_CHOICE, "Poll Multiple Choice"), (POLL_CHECKBOX, "Poll Checkbox"), (POLL_CHOICE, "Poll Choice")
    ]

    TOP_LEVEL = set((QUESTION, DEBATE, META, EDIT_HISTORY, SURVEY, JOB, FORUM, PAGE, BLOG, DATA, TUTORIAL, TOOL, NEWS, BOARD, ARTICLE, TEMPLATE_ARTICLE))
    POLL_TYPES = set((POLL_MULTIPLE_CHOICE, POLL_CHECKBOX, POLL_CHOICE))

    title = models.CharField(max_length=200, null=False)

    # The user that originally created the post.
    author = models.ForeignKey(settings.AUTH_USER_MODEL)

    # The user that edited the post most recently.
    lastedit_user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='editor')

    # Indicates the information value of the post.
    rank = models.FloatField(default=0, blank=True)

    # Post status: open, closed, deleted.
    status = models.IntegerField(choices=STATUS_CHOICES, default=OPEN)
    
    # Vote types: up, up / down, quality, quality + level of agreement, quality + level of agreement + relevance
    vote_types = models.IntegerField(choices=VOTE_TYPES, default=QUALITY)

    # The type of the post: question, answer, comment.
    type = models.IntegerField(choices=TYPE_CHOICES, db_index=True)

    # Number of upvotes minus number of downvotes for the post
    vote_count = models.IntegerField(default=0, blank=True, db_index=True)

    # Average relevance for the post
    relevance = models.FloatField(default=0, blank=True, db_index=True)

    # Number of relevance votes for the post
    relevance_count = models.IntegerField(default=0, blank=True)

    # Average level of agreement for the post
    level_of_agreement = models.FloatField(default=0, blank=True)

    # Number of level of agreement votes for the post
    level_of_agreement_count = models.IntegerField(default=0, blank=True)

    # Average quality for the post
    quality = models.FloatField(default=0, blank=True)

    # Number of quality votes for the post
    quality_count = models.IntegerField(default=0, blank=True)

    # The number of views for the post.
    view_count = models.IntegerField(default=0, blank=True)

    # The number of replies that a post has.
    reply_count = models.IntegerField(default=0, blank=True)

    # The number of comments that a post has.
    comment_count = models.IntegerField(default=0, blank=True)

    # Bookmark count.
    book_count = models.IntegerField(default=0)

    # Indicates indexing is needed.
    changed = models.BooleanField(default=True)

    # How many people follow that thread.
    subs_count = models.IntegerField(default=0)

    # The total score of the thread (used for top level only)
    thread_score = models.IntegerField(default=0, blank=True, db_index=True)

    # Date related fields.
    creation_date = models.DateTimeField(db_index=True)
    lastedit_date = models.DateTimeField(db_index=True)

    # If content is dynamic, the post content must be re-rendered each time it is displayed
    # dynamic_content = models.BooleanField(default=False)

    # Stickiness of the post.
    sticky = models.BooleanField(default=False, db_index=True)

    # Indicates whether the post has accepted answer.
    has_accepted = models.BooleanField(default=False, blank=True)

    # This will maintain the ancestor/descendant relationship bewteen posts.
    root = models.ForeignKey('self', related_name="descendants", null=True, blank=True)

    # This will maintain parent/child replationships between posts.
    parent = models.ForeignKey('self', null=True, blank=True, related_name='children')
    
    # Meta discussions about this post.
    # meta_posts = models.ManyToManyField('self', symmetrical=False, related_name='target_post')

    edit_history = models.ForeignKey('self', related_name="target_post", null=True, blank=True)

    # links pointing on other posts in the post description.
    links_to_others = models.ManyToManyField('self', symmetrical=False, related_name='links_to_me')

    # This is the HTML that the user enters.
    content = models.TextField(default='')

    # This is the  HTML that gets displayed.
    html = models.TextField(default='')

    # The tag value is the canonical form of the post's tags
    tag_val = models.CharField(max_length=100, default="", blank=True)

    # The tag set is built from the tag string and used only for fast filtering
    tag_set = models.ManyToManyField(Tag, blank=True, )

    # What site does the post belong to.
    site = models.ForeignKey(Site, null=True, db_index=True)

    def parse_tags(self):
        return util.split_tags(self.tag_val)

    @staticmethod
    def get_type_from_name(type_name):
        if type_name == 'radio' or type_name == 'multiple choice' or type_name == 'multiple_choice':
            return Post.POLL_MULTIPLE_CHOICE
        elif type_name == 'checkbox':
            return Post.POLL_CHECKBOX
        matches = [type_pair[0] for type_pair in Post.TYPE_CHOICES if type_pair[1] == type_name]
        return matches[0] if len(matches) > 0 else None

    def add_tags(self, text):
        text = text.strip()
        if not text:
            return
        # Sanitize the tag value
        self.tag_val = bleach.clean(text, tags=[], attributes=[], styles={}, strip=True)
        # Clear old tags
        self.tag_set.clear()
        tags = [Tag.objects.get_or_create(name=name, site=self.site)[0] for name in self.parse_tags()]
        self.tag_set.add(*tags)
        #self.save()

    @property
    def as_text(self):
        "Returns the body of the post after stripping the HTML tags"
        text = bleach.clean(self.content, tags=[], attributes=[], styles={}, strip=True)
        return text

    def peek(self, length=300):
        "A short peek at the post"
        return self.as_text[:length]

    def get_title(self):
        if self.status == Post.OPEN:
            return self.title
        else:
            return "[%s] %s" % ( self.get_status_display(), self.title)

    @property
    def get_sticky(self):
        return "sticky" if self.sticky else ""

    @property
    def is_article(self):
        return self.type == Post.ARTICLE

    @property
    def is_open(self):
        return self.status == Post.OPEN

    @property
    def age_in_days(self):
        delta = const.now() - self.creation_date
        return delta.days

    def update_reply_count(self):
        "This can be used to set the answer count."
        post_type = int(self.type) if self.type else None
        if post_type == Post.ANSWER or post_type == Post.ARGUMENT_FOR or post_type == Post.ARGUMENT_AGAINST:
            reply_count = Post.objects.filter(parent=self.parent, type__in=[Post.ANSWER, Post.ARGUMENT_FOR, Post.ARGUMENT_AGAINST], status=Post.OPEN).count()
            Post.objects.filter(pk=self.parent_id).update(reply_count=reply_count)

    def delete(self, using=None):
        # Collect tag names.
        tag_names = [t.name for t in self.tag_set.all()]

        # While there is a signal to do this it is much faster this way.
        Tag.objects.filter(name__in=tag_names).update(count=F('count') - 1)

        # Remove tags with zero counts.
        Tag.objects.filter(count=0).delete()
        super(Post, self).delete(using=using)

    def save(self, request=None, parse_html=True, *args, **kwargs):

        # Sanitize the post body.
        if parse_html:
            self.html = html.parse_html(self.content, self, request=request)

        # Must add tags with instance method. This is just for safety.
        self.tag_val = html.strip_tags(self.tag_val)

        # Posts other than a question also carry the same tag
        post_type = int(self.type) if self.type else None
        if self.is_toplevel and post_type != Post.QUESTION:
            required_tag = self.get_type_display()
            if required_tag not in self.tag_val:
                self.tag_val += "," + required_tag

        if not self.id:

            # Set the titles
            if self.parent and not self.title:
                self.title = self.parent.title

            if self.parent and self.parent.type in (Post.ANSWER, Post.ARGUMENT_FOR, Post.ARGUMENT_AGAINST, Post.COMMENT):
                # Only comments may be added to a parent that is answer or comment.
                self.type = Post.COMMENT
            
            # Set post type if it was left empty.
            if self.type is None:
                self.type = self.COMMENT if self.parent else self.FORUM
            elif self.type == '':
                self.type = self.ANSWER

            # This runs only once upon object creation.
            self.title = self.parent.title if self.parent and (self.title is None or len(self.title) == 0) else self.title
            self.lastedit_user = self.author
            self.status = self.status or Post.PENDING
            self.creation_date = self.creation_date or now()
            self.lastedit_date = self.creation_date

            post_type = int(self.type)
            # Set the timestamps on the parent
            if post_type == Post.ANSWER or post_type == Post.ARGUMENT_FOR or post_type == Post.ARGUMENT_AGAINST:
                # self.parent.lastedit_date = self.lastedit_date
                # self.parent.lastedit_user = self.lastedit_user
                self.parent.save()

        # Recompute post reply count
        self.update_reply_count()

        super(Post, self).save(*args, **kwargs)

    def __unicode__(self):
        return "%s: %s (id=%s)" % (self.get_type_display(), self.title, self.id)

    @property
    def is_toplevel(self):
        return self.type in Post.TOP_LEVEL

    @property
    def is_poll(self):
        return self.type in Post.POLL_TYPES

    def get_absolute_url(self):
        "A blog will redirect to the original post"
        #if self.url:
        #    return self.url
        url = reverse("post-details", kwargs=dict(pk=self.root_id))
        return url if self.is_toplevel else "%s#%s" % (url, self.id)

    @staticmethod
    def update_post_views(post, request, minutes=settings.POST_VIEW_MINUTES):
        "Views are updated per user session"

        # Extract the IP number from the request.
        ip1 = request.META.get('REMOTE_ADDR', '')
        ip2 = request.META.get('HTTP_X_FORWARDED_FOR', '').split(",")[0].strip()
        # 'localhost' is not a valid ip address.
        ip1 = '' if ip1.lower() == 'localhost' else ip1
        ip2 = '' if ip2.lower() == 'localhost' else ip2
        ip = ip1 or ip2 or '0.0.0.0'

        now = const.now()
        since = now - datetime.timedelta(minutes=minutes)

        # One view per time interval from each IP address.
        if not PostView.objects.filter(ip=ip, post=post, date__gt=since):
            PostView.objects.create(ip=ip, post=post, date=now)
            Post.objects.filter(id=post.id).update(view_count=F('view_count') + 1)
        return post

    @staticmethod
    def check_root(sender, instance, created, *args, **kwargs):
        "We need to ensure that the parent and root are set on object creation."
        if created:

            if not (instance.root or instance.parent):
                # Neither root or parent are set.
                instance.root = instance.parent = instance

            elif instance.parent:
                # When only the parent is set the root must follow the parent root.
                instance.root = instance.parent.root

            elif instance.root:
                # The root should never be set on creation.
                raise Exception('Root may not be set on creation')

            if instance.parent.type in (Post.ANSWER, Post.ARGUMENT_FOR, Post.ARGUMENT_AGAINST, Post.COMMENT) and instance.type not in Post.POLL_TYPES:
                # Answers and comments may only have comments associated with them.
                instance.type = Post.COMMENT

            if (not instance.root) or (not instance.parent):
                import pdb; pdb.set_trace()

            assert instance.root and instance.parent

            if not instance.is_toplevel:
                # Title is inherited from top level.
                instance.title = "%s: %s" % (instance.get_type_display()[0], instance.root.title[:80])

                instance_type = int(instance.type) if instance.type else None

                if instance_type == Post.ANSWER or instance_type == Post.ARGUMENT_FOR or instance_type == Post.ARGUMENT_AGAINST:
                    Post.objects.filter(id=instance.root.id).update(reply_count=F("reply_count") + 1)

            instance.save()

    @staticmethod
    def initialize_augmented_links(sender, instance, created, **kwargs):
        "Initialize augmented links on post creation and edition."
        
        if created and not getattr(instance, '_disable_signals', False):
            # Parse content to extract links to other posts and save them to links_to_others.
            # Ignore edit histories (no need to see mentions from the edit histories)
            if instance.type != Post.EDIT_HISTORY and instance.type != Post.EDIT:
                instance.links_to_others.clear()
                for post in html.get_links_to_others(instance.html):
                    instance.links_to_others.add(post)
                instance.save_without_signals()
    
    def check_close_survey(self):

        if self.type == Post.SURVEY:
            try:
                survey = Survey.objects.get(post=self)
                if const.now() > survey.close_date:
                    self.status = Post.CLOSED
                    self.save()
                return survey.close_date
            except ObjectDoesNotExist, exc:
                pass

        return None

    def is_closed_survey(self):

        if self.type == Post.SURVEY:
            
            self.check_close_survey()
            if self.status == Post.CLOSED:
                return True

        return False

class ReplyToken(models.Model):
    """
    Connects a user and a post to a unique token. Sending back the token identifies
    both the user and the post that they are replying to.
    """
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    post = models.ForeignKey(Post)
    token = models.CharField(max_length=256)
    date = models.DateTimeField(auto_created=True)

    def save(self, *args, **kwargs):
        if not self.id:
            self.token = util.make_uuid()
        super(ReplyToken, self).save(*args, **kwargs)

class ReplyTokenAdmin(admin.ModelAdmin):
    list_display = ('user', 'post', 'token', 'date')
    ordering = ['-date']
    search_fields = ('post__title', 'user__name')

admin.site.register(ReplyToken, ReplyTokenAdmin)


class EmailSub(models.Model):
    """
    Represents an email subscription to the digest digest.
    """
    SUBSCRIBED, UNSUBSCRIBED = 0, 1
    TYPE_CHOICES = [
        (SUBSCRIBED, "Subscribed"), (UNSUBSCRIBED, "Unsubscribed"),

    ]
    email = models.EmailField()
    status = models.IntegerField(choices=TYPE_CHOICES)


class EmailEntry(models.Model):
    """
    Represents an digest digest email entry.
    """
    DRAFT, PENDING, PUBLISHED = 0, 1, 2

    # The email entry may be posted as an entry.
    post = models.ForeignKey(Post, null=True)

    # This is a simplified text content of the Post body.
    text = models.TextField(default='')

    # The data the entry was created at.
    creation_date = models.DateTimeField(auto_now_add=True)

    # The date the email was sent
    sent_at = models.DateTimeField(null=True, blank=True)

    # The date the email was sent
    status = models.IntegerField(choices=((DRAFT, "Draft"), (PUBLISHED, "Published")))


class PostAdmin(admin.ModelAdmin):
    list_display = ('title', 'type', 'author', 'id')
    fieldsets = (
        (None, {'fields': ('title',)}),
        ('Attributes', {'fields': ('type', 'status', 'sticky',)}),
        ('Content', {'fields': ('content', )}),
    )
    search_fields = ('title', 'author__name', 'id')

admin.site.register(Post, PostAdmin)


class PostView(models.Model):
    """
    Keeps track of post views based on IP address.
    """
    ip = models.GenericIPAddressField(default='', null=True, blank=True)
    post = models.ForeignKey(Post, related_name="post_views")
    date = models.DateTimeField(auto_now=True)


class Vote(models.Model):
    # Post statuses.
    UP, DOWN, RELEVANCE, LEVEL_OF_AGREEMENT, QUALITY, BOOKMARK, ACCEPT, POLL = range(8)
    TYPE_CHOICES = [(UP, "Up Vote"), (DOWN, "Down Vote"), (RELEVANCE, "Relevance"), (LEVEL_OF_AGREEMENT, "Level Of Agreement"), (QUALITY, "Quality"), (BOOKMARK, "Bookmark"), (ACCEPT, "Accept"), (POLL, "Poll")]

    author = models.ForeignKey(settings.AUTH_USER_MODEL)
    post = models.ForeignKey(Post, related_name='votes')
    type = models.IntegerField(choices=TYPE_CHOICES, db_index=True)
    # type = models.IntegerField(db_index=True)
    value = models.IntegerField(default=1)
    date = models.DateTimeField(db_index=True, auto_now=True)

    def __unicode__(self):
        return u"Vote: %s, %s, %s" % (self.post_id, self.author_id, self.get_type_display())

class VoteAdmin(admin.ModelAdmin):
    list_display = ('author', 'post', 'type', 'date')
    ordering = ['-date']
    search_fields = ('post__title', 'author__name')

admin.site.register(Vote, VoteAdmin)

class SubscriptionManager(models.Manager):
    def get_subs(self, post):
        "Returns all suscriptions for a post"
        return self.filter(post=post.root).select_related("user")

# This contains the notification types.
from biostar.const import LOCAL_MESSAGE, MESSAGING_TYPE_CHOICES


class Subscription(models.Model):
    "Connects a post to a user"

    class Meta:
        unique_together = (("user", "post"),)

    user = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=_("User"), db_index=True)
    post = models.ForeignKey(Post, verbose_name=_("Post"), related_name="subs", db_index=True)
    type = models.IntegerField(choices=MESSAGING_TYPE_CHOICES, default=LOCAL_MESSAGE, db_index=True)
    date = models.DateTimeField(_("Date"), db_index=True)

    objects = SubscriptionManager()

    def __unicode__(self):
        return "%s to %s" % (self.user.name, self.post.title)

    def save(self, *args, **kwargs):

        if not self.id:
            # Set the date to current time if missing.
            self.date = self.date or const.now()

        super(Subscription, self).save(*args, **kwargs)


    @staticmethod
    def get_sub(post, user):

        if user.is_authenticated():
            try:
                return Subscription.objects.get(post=post, user=user)
            except ObjectDoesNotExist, exc:
                return None

        return None

    @staticmethod
    def create(sender, instance, created, *args, **kwargs):
        "Creates a subscription of a user to a post"
        
        if getattr(instance, '_disable_signals', False):
            return

        user = instance.author
        root = instance.root

        if Subscription.objects.filter(post=root, user=user).count() == 0:
            sub_type = user.profile.message_prefs
            if sub_type == const.DEFAULT_MESSAGES:
                sub_type = const.EMAIL_MESSAGE if instance.is_toplevel else const.LOCAL_MESSAGE
            sub = Subscription(post=root, user=user, type=sub_type)
            sub.date = datetime.datetime.utcnow().replace(tzinfo=utc)
            sub.save()
            # Increase the subscription count of the root.
            Post.objects.filter(pk=root.id).update(subs_count=F('subs_count') + 1)

    @staticmethod
    def finalize_delete(sender, instance, *args, **kwargs):
        # Decrease the subscription count of the post.
        Post.objects.filter(pk=instance.post.root_id).update(subs_count=F('subs_count') - 1)



# Admin interface for subscriptions
class SubscriptionAdmin(admin.ModelAdmin):
    search_fields = ('user__name', 'user__email')
    list_select_related = ["user", "post"]


admin.site.register(Subscription, SubscriptionAdmin)

class Survey(models.Model):
    post = models.ForeignKey(Post, related_name='survey', db_index=True)

    creation_date = models.DateTimeField(db_index=True, auto_now=True)
    close_date = models.DateTimeField(db_index=True)

    # def __unicode__(self):
        # return u"Survey: %s, creation date: %s, close date: %s" % (self.post_id, self.creation_date, self.close_date)

# Admin interface for surveys
class SurveyAdmin(admin.ModelAdmin):
    list_display = ('post', 'creation_date', 'close_date', 'id')
    ordering = ('-creation_date', 'close_date', )

admin.site.register(Survey, SurveyAdmin)

class SiteSettings(models.Model):
    site = models.ForeignKey(Site, null=True, db_index=True)
    site_domain = models.CharField(max_length=50, null=False)
    language = models.CharField(max_length=50, null=False)
    timezone = models.CharField(max_length=50, null=False)

class SiteSettingsAdmin(admin.ModelAdmin):
    list_display = ('site', 'language', 'timezone')

admin.site.register(SiteSettings, SiteSettingsAdmin)

# To update the database, syncdb, apply migrations:

# source conf/debug.env
# source conf/defaults.env
# (or the other way arround...)
# python manage.py schemamigration --auto posts
# python manage.py migrate posts

# Data signals
from django.db.models.signals import post_save, post_delete, m2m_changed

post_save.connect(Post.check_root, sender=Post, dispatch_uid="check_root")
post_save.connect(Post.initialize_augmented_links, sender=Post, dispatch_uid="initialize_augmented_links")
post_save.connect(Subscription.create, sender=Post, dispatch_uid="create_subs")
post_delete.connect(Subscription.finalize_delete, sender=Subscription, dispatch_uid="delete_subs")
m2m_changed.connect(Tag.update_counts, sender=Post.tag_set.through)
