��    �      �  �   ,      �  �   �  .  �  �   �  R   �  E   �  �   9  �   �    �  �   �  i   q  a  �  �   =    �  ^   �  I   R  Y   �  �   �     �       <   0  \   m  )   �  1   �  ;   &  d   b  >   �  ,     
   3     >  `   E  _   �  f     5   m     �     �  L   �            	   #  	   -     7     G  	   O     Y     j     o     w     �  1   �     �     �     �     �     �     
               	   3      =   p   N   2   �      �       �      !     :!     A!     U!     \!     d!     i!     q!     z!     �!  	   �!     �!     �!     �!     �!     �!     �!     �!  	   �!     �!     "  	   "  ?   "  A   Q"     �"     �"     �"     �"     �"     �"     �"     �"     �"     #     #     #     ;#     X#     f#     y#     �#     �#     �#     �#  �   �#  D   q$     �$     �$     �$  D   �$  p   %  '   �%  �   �%     m&     �&      �&     �&     �&     �&     �&  )   �&     ''  	   0'     :'     ='     M'     V'     e'     q'     u'     }'     �'     �'  .   �'  
   �'  	   �'     �'     �'     �'     �'     �'     (     (     (     '(     -(     2(  <   @(  =   }(  "   �(  
   �(     �(     �(     �(     �(  	   )     )  
   )     %)     -)  
   5)  	   @)  	   J)     T)     Z)     `)     g)     m)     s)     x)  
   �)     �)     �)  �   �)  i   �*  j  �*  �   h,  ]   -  U   x-  �   �-  �   �.     s/  �   t0  �   S1  �  �1  �   �3  d   4  z   �5  W    6  r   X6    �6      �7     8  <   $8  Y   a8  )   �8  4   �8  9   9  �   T9  ?   �9  3   :  	   M:  	   W:  a   a:  `   �:  q   $;  6   �;     �;     �;  e   �;     T<     ]<     t<     �<     �<     �<     �<     �<     �<     �<     �<     �<  H   �<     ?=     ^=     g=     w=     =     �=     �=     �=     �=     �=  �   �=  C   h>  	   �>     �>     �>     �>     �>  	   ?     ?     %?     *?     2?     ;?     I?     W?     e?     |?  !   �?     �?     �?     �?     �?  	   �?     �?     �?     �?  G   @  E   L@     �@  	   �@     �@     �@     �@  	   �@     �@     A     A     (A     8A  *   EA  +   pA     �A     �A     �A     �A     �A     �A     �A  �   
B  =   �B     1C     AC     MC  H   SC  {   �C  )   D  �   BD  )   )E     SE  *   mE  $   �E     �E  
   �E  	   �E     �E      F     F     F     F  	   6F     @F     RF     eF     jF     wF  
   �F     �F  @   �F  
   �F     �F     �F     �F     G     G     G     &G     -G     6G     ?G     DG  
   LG  =   WG  >   �G  &   �G  	   �G     H     H     H     H  
   /H  	   :H     DH  
   PH  
   [H  
   fH     qH     H     �H     �H     �H     �H     �H     �H     �H     �H     �H  	   �H                   v   @       w   Y   �   `       !   �      �   B   �   �       '      �   K   _   	   �   b       *   W   S   n   3              ]   �       6   N   s   r   h   $          z                  G   a       0   �           =   4   U              �       I       �   [   �                     �          #   �   :       �   d   �   ~           9          t       �      �   �   /   {      "   V       �   l       D             .   1   R   Q   \   �   o   |           Z      -   M   O   +   i         2   �   �   (   �   ^   L              �   �   k       ;   �   �       �   
   X          �   C       >   8   �       F   �       �       p   &           �   %   x   �   A   )   �       u           T       <       �   �   j   f              q      5   �   �      �   g   m   �   �   �          c                P   �   �      �                   E   e   ,       H   }   �   �   7   y   �           J       ?        

<p>Hello %(user_name)s!</p>

<p>It appears that your post has been cross-posted to another site: %(comment)s</p>

<p>This is typically not recommended as it runs the risk of annoying people in both communities.</p>

 

Hello.

You're receiving this e-mail because you or someone else has
requested a password reset for your user account at %(site_domain)s.

Click the link below to reset your password.

%(password_reset_url)s

Ignore the message if you did not request this password reset.

Thanks for using our site!
 

Welcome to %(site_name)s!

You are now one step closer to become a full member of our community.

To verify your email please click on the link below.

%(activate_url)s

 
                            Click the name of a provider to log in or to sign up. 
                            Click the name of a provider to sign up. 
                            This is used for authentication only.
                            We can't access your timeline, see your friends or post on your behalf.
                         
                        The site has <b>%(user_count|intcomma)s</b> registered users that created
                        <b>%(post_count|intcomma)s</b> posts</li>
                         
                        These are distributed as <b>%(question_count|intcomma)s</b>
                        questions,<b> %(answer_count|intcomma)s</b> answers
                        and <b>%(comment_count|intcomma)s</b> comments.
                         
                        This is used for authentication only.
                        We can't access your timeline, see your friends or post on your behalf.
                         
    If you disagree with this please tell us why in a reply below. We'll be happy to talk about it.
     
    Please check that users behave responsibly. If you see any abuse, you can close the post and open a survey so that the community defines the content of the post. Anybody will be able to make proposals and vote for them. At the end of the survey, you will be able to edit the post according to the best proposition and you can re-open the post.
     
    Together, by sharing the know-how, personal experiences, tips and tricks of the trade
    we can become more productive and successful as scientists.
     
    We encourage you to participate:
    <b>upvote</b> posts that you like, ask <b>questions</b>, write <b>comments</b> and <b>answers</b>, follow posts,
    bookmark content. You can keep up with the lates content via email (see your user account settings) or RSS feeds.
     
    We have closed your question to allow us to keep similar content in the same thread.
     
    We hope you'll find the site useful and our community friendly.
     
    We noticed frequent edit on the post <a href='%(post_url)s'>%(post_title)s</a>.
     
<p>For this reason we have closed your question. This allows us to keep the site focused
on the topics that the community can help with.</p>

<p>If you disagree please tell us why in a reply below, we'll be happy to talk about  it.</p>

<p>Cheers!</p>
 
<p>Hello %(user_name)s!</p>
 
<p>Hello %(user_name)s,</p>
 
<p>Questions similar to yours can already be found at:</p>
 
Activity on a post you are following on <a href="http://%(site_domain)s">%(site_name)s</a>
 
Biostars, home of %(post_count)s posts.
 
Welcome to Biostar <i class="fa fa-globe"></i>!
 
You have successfully signed in as <b>%(user_email)s</b>.
  Classic search. Type your query and press enter. Go to the main page            for dynamic search.  page %(page_obj.number)s of %(page_obj.paginator.num_pages)s  %(page_obj.paginator.count|intcomma)s users  %0.1f year %s ago <div class="alert alert-info convert-time">This survey ended on the %(survey_close_date)s.</div> <div class="alert alert-info convert-time">This survey ends on the %(survey_close_date)s.</div> <p>This survey ended the %(survey_close_date)s. It is not possible to vote in this thread anymore.</p> <p>This survey ends on the %(survey_close_date)s.</p> About Administrator access: An error occurred while attempting to login via your social network account. Articles Back to login Bad Token Bookmarks Change Password Cheers! Community Congratulations! Date Debates Discussions Do not follow Do you have an email based account? Log in below: Do you need an account? Edit Email Login English Enter a whole number. FAQ Follow via email Follow via messages Following Forgot Password? Forgotten your password? Enter your e-mail address below, and we'll send you an e-mail allowing you to reset it. Form based signup is now disabled because of spam. French Invalid limit parameter received Invalid sort parameter received Latest Limit to: %(limit)s Log In Log Out META Message Messages New Blog Posts New Post New Posts No results found. Parent message Password Reset Please log in Post Posts Profile Questions Quick Login RSS Recipient Replying to this email will post a comment to the answer above. Replying to this email will post an answer to the question above. Reset password Search See the full post at: Sender Server Stats Sign In Sign Up Sign Up Here Sign Up with Email Site Admins Site Moderators Social Network Login Cancel Social Network Login Failure Social Signup Sort by: %(sort)s  Subject Surveys Tags Text The login has been cancelled. The password reset link was invalid, possibly because it has already been used.
                    Please
                    request a <a href="%(passwd_reset_url)s">new password reset</a>. Traffic: <b>%(TRAFFIC|intcomma)s</b> users visited in the last hour. Updated Posts User Votes We believe that this post does not fit the main topic of this site.  We have sent you an e-mail. Please contact us if you do not receive it within a few
                    minutes. What is %(num1)i %(operator)s %(num2)i? You are about to use
                    your %(provider_name)s account
                    to login to
                    %(site_name)s. As a final step, please complete the following form: You have earned this honor for: You have won the You may reply via email or visit Your password is now changed. activity level all time answers badge for the notable accomplishment of:  bookmark bookmarks by change password creation creation dates date joined day default edit edit history email email for every new thread (mailing list mode) follow via followers hour just now level of agreement link local messages minute moderate modified month next not following page %(page_obj.number)s of %(page_obj.paginator.num_pages)s page %(page_obj.number)s of %(page_obj.paginator.num_pages)s  please confirm your e-mail address posted the prev quality rank recent visit relevance replies reputation results sent at this month this week this year today types update views votes week written written by wrote wrote on Project-Id-Version: 1
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-11-06 17:08+0100
Last-Translator: 
Language-Team: 
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.2.4
 

<p>Bonjour %(user_name)s !</p>

<p>Il semble que vous ayez déjà posté ce message : %(comment)s</p>

 

Bonjour.

Vous recevez cet email parce que vous (ou quelqu'un d'autre) avez 
demandé la réinitialisation de votre mot de passe sur %(site_domain)s.

Cliquez sur le lien ce dessous pour réinitialiser votre mot de passe.

%(password_reset_url)s

Vous pouvez ignorer ce message si vous n'êtes pas à l'origine de cette demande.

Merci d'utiliser notre site !
 

Bienvenue sur %(site_name)s !

Vous allez bientôt devenir membre de notre communauté.

Pour valider votre email, veuillez cliquer sur le lien ci-dessous:

%(activate_url)s

 
                            Cliquez sur un compte pour vous connecter ou pour vous inscrire. 
                            Cliquer sur le nom de la plateforme pour vous connecter. 
                            Ce système est uniquement utilisé pour l'authentification.
                            Le site ne pourra pas accéder à votre timeline, vos amis ou vos publications.
                         
                        Le site a <b>%(user_count|intcomma)s</b> utilisateurs enregistrés qui ont créé
                        <b>%(post_count|intcomma)s</b> posts</li>
                         
                        Il y a <b>%(question_count|intcomma)s</b>
                        questions hébergées,<b> %(answer_count|intcomma)s</b> réponses
                        et <b>%(comment_count|intcomma)s</b> commentaires.
                         
                            Ce système est uniquement utilisé pour l'authentification.
                            Le site ne pourra pas accéder à votre timeline, vos amis ou vos publications.
                         
    Si vous n'êtes pas d'accord avec ce fonctionnement dans ce cas précis, veuillez envoyer une réponse ci-dessous. Nous sommes ouvert pour en parler.
     
    Veuillez vérifier que les utilisateurs responsables se comportent correctement. Si vous observez un abus, vous pouvez fermer ce post et ouvrir un sondage pour que la communauté définisse le contenu du post. Tout le monde pourra faire des propositions et voter pour celles-ci. A la fin du sondage, vous pourrez modifier le post selon les meilleurs propositions et vous pourrez réouvrir le post.
     
    Ensemble, en partageant nos connaissance, notre experience et nos astuces
    nous pouvons devenir plus cohérent et mieux nous organiser.
     
    Nous vous encourageons à participer :
    <b>votez</b> pour les posts que vous aimez, posez des <b>questions</b>, écrivez des <b>commentaires</b> et des <b>réponses</b>, suivez des posts,
    ajoutez des marque-pages. Vous pouvez vous recevoir les dernières actualités par email (voir dans les préférences de votre compte) ou par flux RSS.
     
    Nous avons fermé cette question pour permettre de garder le contenu similaire dans le  même fil de discussion.
     
    Nous esperons que vous trouverez ce site utile et la communauté sympathique.
     
    Nous avons observé des modifications fréquentes sur le post <a href='%(post_url)s'>%(post_title)s</a>.
     
<p>Nous avons donc fermé votre question. Cela nous permet de garder le site cohérent et focalisé
sur les sujets que la communauté maitrise.</p>

<p>Si vous n'êtes pas d'accord, veuillez écrire une réponse ci-dessous, nous serons ouvert pour en parler.</p>

<p>Merci!</p>
 
<p>Bonjour %(user_name)s !</p>
 
<p>Bonjour %(user_name)s,</p>
 
<p>Des questions similaires existent à ces adresses :</p>
 
Activié du post que vous suivez sur <a href="http://%(site_domain)s">%(site_name)s</a>
 
Biostars héberge %(post_count)s posts.
 
Bienvenu sur Biostar <i class="fa fa-globe"></i> !
 
Vous êtes connecté en tant que <b>%(user_email)s</b>.
 Recherche classique. Entrez votre requête et validez. Vous pouvez 
            faire une recherche dynamique sur la page d'accueil.  page %(page_obj.number)s sur %(page_obj.paginator.num_pages)s  %(page_obj.paginator.count|intcomma)s utilisateurs  %0.1f ans il y a %s <div class="alert alert-info convert-time">Ce sondage à pris fin le %(survey_close_date)s.</div> <div class="alert alert-info convert-time">Ce sondage se termine le %(survey_close_date)s.</div> <p>Ce sondage à pris fin le %(survey_close_date)s. Il n'est plus possible de voterpour ce fil de discussion.</p> <p>Ce sondage se termine le %(survey_close_date)s.</p> A propos Accès administrateur : Une erreur est servenue lors de la tentative de connexion avec votre compte sur la plateforme tierce. Articles Retour à la connexion Mauvais numéro Marque-pages Modifier le mot de passe Merci ! Communauté Félicitations ! Date Débats Discussions Ne pas suivre Vous avez créé un compte avec votre email ? Connectez vous ci-dessous: Avez vous besoin d'un compte ? Modifier Connexion email Anglais Entrez un nombre entier. FAQ Suivre par email Suivre par messages Posts suivis Mot de passe oublié ? Mot de passe oublié ? Entrez votre adresse email ci-dessous, et un mail vous sera envoyé pour réinitialiser votre mot de passe. Les inscriptions via formulaire sont désactivée à cause du spam. Français Paramètre de limite invalide Paramètre de tri invalide Nouveaux Limiter à: %(limit)s Connexion Déconnexion META Message Messages Nouveau Posts Nouveau Posts Nouveau Posts Aucun résulat trouvé Message parent Réinitialisation du mot de passe Veuillez vous connecter Post Posts Profile Questions Connexion rapide RSS Destinataire Répondre à cet email créera un commentaire à la réponse ci-dessus. Répondre à cet email créera une réponse à la question ci-dessus. Réinitialiser le mot de passe Recherche Voir la réponse complète à : Emetteur Statistiques Connexion Inscription Inscription S'inscrire par email Administrateurs Modérateurs Annuler la connexion via plateforme tierce Echec de la connexion via plateforme tierce Connexion via plateforme tierce Trier par: %(sort)s  Sujet Sondages Tags Texte La connexion a été annulée. Le lien de réinitialisation du mot de passe n'est pas valide, probablement parce qu'il a été utilisé.
                    Veuillez demander un <a href="%(passwd_reset_url)s">nouveau lien de réinitialisation</a> de mot de passe. Trafic: <b>%(TRAFFIC|intcomma)s</b> visites depuis une heure. Posts modifiés Utilisateur Votes Nous pensons que ce post n'est pas compatible avec le sujet de ce site.  Nous vous avons envoyé un email. Si vous ne recevez pas le message dans les minutes qui viennent, veuillez nous contacter. Quel est %(num1)i %(operator)s %(num2)i ? Vous êtes sur le point d'utiliser
                    votre compte %(provider_name)s 
                    pour vous connecter sur
                    %(site_name)s. En dernière étape, veuillez compléter  le formulaire suivant: Vous avez obtenu cet honneur pour avoir : Vous avez obtenu le badge Vous pouvez répondre par email ou visiter Votre mot de passe a été modifié. niveau d'activité tout temps réponses pour votre réalisation :  marque-page marque-pages par modifier le mot de passe création date de création date d'inscription jour par défault modifié historique email email pour chaque nouveau fil de discussion (mode mailing liste) suivre par utilisateurs inscrits heure à l'instant accord lien Suivre par messages minute modérer modifié mois suivant non suivis page %(page_obj.number)s sur %(page_obj.paginator.num_pages)s page %(page_obj.number)s sur %(page_obj.paginator.num_pages)s  veuillez confirmer votre adresse email posté le précédent qualité rang visite récente pertinence réponses réputation résultats envoyé à ce mois ci cette semaine cette année aujourd'hui types mettre à jour vues votes semaine écrit écrit écrit écrit le 