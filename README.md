## Collaborative Platform

[![License](http://img.shields.io/:license-mit-blue.svg)](http://doge.mit-license.org)

The collective reflection platform aims at collecting, agregating and sharing ideas to help creating collaborative reflections.
It is self-improving, meaning that its main objective is to provide tools and cultures to design online reflection.

Technically, the platform is a fork of [BioStar][biostar] extended with a few features to create debates and surveys.

The software is open source and free to use under the MIT License.

### Features

See the full list of features here.

* Q&A: questions, answers, comments, user moderation, voting, reputation, badges, threaded discussions
* Debates: arguments pro / against / neutral to the thesis
* Polls: set a deadline after which it is not possible to vote anymore
* Experimental ratings: rate the quality, your level of agreement and the relevance of posts
* Templates: same as [wikipedia templates](https://en.wikipedia.org/wiki/Help:Template)
* External authentication: authenticate users with a different web service

Since the collaborative reflection platform is similar to [BioStar](biostar), you can refer to the BioStar documentation:


Requirements: `Python 2.7`

### Documentation

* [Install](docs/install.md)
* [Manage](docs/manage.md)
* [Deploy](docs/deploy.md)
* [Customize](org/bioconductor/README.md)

The source for the documentation can be found in  the [docs](./docs) folder.

### Quick Start

From the biostar source directory:

    # Install the requirements.
    pip install --upgrade -r conf/requirements/base.txt

    # Initialize database, import test data, index for searching and run the server.
    ./biostar.sh init import index run

Visit `http://www.lvh.me:8080` to see the site loaded with demo data.

The `www.lvh.me` domain resolves to `127.0.0.1` your local host 
with a proper domain name. You may just as well use `http://localhost:8080` or `http://127.0.0.1`.

In the demo site the user emails are built from the database ids like so: `1@lvh.me`, `2@lvh.me`.
The demo user passwords are identical to the emails
and you may use these to log into your test site as any of the users.

The user with the email `1@lvh.me` has staff level permissions and
can also access the admin interface at `http://www.lvh.me:8080/admin/`.

Enjoy.

### Development

Biostar versions and upgrade path: https://github.com/ialbert/biostar-central/issues/400

### Support

We may be able to provide support for organizations or institutions. 
For more information contact **admin@biostars.org**

[django]: http://www.djangoproject.com/
[python]: http://www.python.org/
[biostar]: https://www.biostars.org/

### Citing

* Parnell LD, Lindenbaum P, Shameer K, Dall'Olio GM, Swan DC, et al.
  [2011 BioStar: An Online Question & Answer Resource for the Bioinformatics Community.] (http://www.ploscompbiol.org/article/info%3Adoi%2F10.1371%2Fjournal.pcbi.1002216)
  PLoS Comput Biol 7(10): e1002216. doi:10.1371/journal.pcbi.1002216

### Contributors

List of contributors: https://github.com/ialbert/biostar-central/graphs/contributors
